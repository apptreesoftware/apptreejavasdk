package com.apptree.sdk.internal;

import com.apptree.sdk.datasource.*;
import com.apptree.sdk.datasource.response.ATConfigurationResponse;
import com.apptree.sdk.datasource.response.ATDataSourceResponse;
import com.apptree.sdk.datasource.response.ATListDataSourceResponse;
import com.apptree.sdk.datasource.response.ATAssessmentSessionStartResponse;
import com.apptree.sdk.model.*;
import com.apptree.sdk.util.JSONUtils;
import com.danisola.restify.url.RestParser;
import com.danisola.restify.url.RestParserFactory;
import com.danisola.restify.url.RestUrl;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.json.JSONObject;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Matthew Smith, AppTree Software LLC on 4/17/15.
 */

/**
 * For use when implemented a to do feature. Examples include audits, condition assesments.
 */

final class ATAssessmentRestHandler extends ATRESTHandler {

    private ATAssessmentDataSource mToDoDataSource;
    public ATAssessmentRestHandler(ATAssessmentDataSource dataSource) {
        mToDoDataSource = dataSource;
    }

    /**
     * The Get method for Assessments prints configuration for an assessment data source in the HttpServletResponse output stream for the path RESTPath()/describe
     * @param servlet
     * @param request
     * @param response
     * @param authInfo
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {

        RestParser restParser = RestParserFactory.parser("/" + RESTPath() + "/" + "describe");
        RestUrl restUrl = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restUrl.isValid() ) {
            ATConfigurationResponse configuration = mToDoDataSource.getConfiguration();
            response.getOutputStream().print(configuration.toJSON().toString());
        }

    }

    /**
     * The post method for an assessment data source; /data returns the assessment data, /start starts an assessment,
     * /cancel calcels an assesment, /end ends an assessment, /save saves an assessment, /submit submits an assessment
     * @param servlet An HttpServlet
     * @param request The servlet request
     * @param response The servlet response
     * @param authInfo A HashMap of any authorization parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        RestParser restParser;
        RestUrl restUrl;

        restParser = RestParserFactory.parser("/" + RESTPath() + "/data");
        restUrl = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restUrl.isValid() ) {
            getExecutor().submit(new GetDataSetTask(request.startAsync(),authInfo,getURLParams(request)));
            return;
        }

        restParser = RestParserFactory.parser("/" + RESTPath() + "/start");
        restUrl = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restUrl.isValid() ) {
            getExecutor().submit(new StartTask(request.startAsync(),authInfo,getURLParams(request)));
            return;
        }

        restParser = RestParserFactory.parser("/" + RESTPath() + "/cancel");
        restUrl = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restUrl.isValid() ) {
            getExecutor().submit(new CancelTask(request.startAsync(),authInfo,getURLParams(request)));
            return;
        }

        restParser = RestParserFactory.parser("/" + RESTPath() + "/end");
        restUrl = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restUrl.isValid() ) {
            getExecutor().submit(new DoneTask(request.startAsync(),authInfo,getURLParams(request)));
            return;
        }

        restParser = RestParserFactory.parser("/" + RESTPath() + "/save");
        restUrl = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restUrl.isValid() ) {
            getExecutor().submit(new SaveTask(request.startAsync(),authInfo,getURLParams(request)));
            return;
        }

        restParser = RestParserFactory.parser("/" + RESTPath() + "/submit");
        restUrl = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restUrl.isValid() ) {
            getExecutor().submit(new SubmitTask(request.startAsync(),authInfo,getURLParams(request),servlet));
            return;
        }
    }

    @Override
    public void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {

    }

    /***
     * Returns the REST path endpoint from the data source
     * @return A string that you want to use for the endpoint defined in your data source
     */
    @Override
    public String RESTPath() {
        return mToDoDataSource.dataSourceRESTPath();
    }


    private class GetDataSetTask extends RequestTask {
        public GetDataSetTask(AsyncContext mContext, HashMap<String, String> authParams, HashMap<String, String> params) {
            super(mContext, authParams, params);
        }

        @Override
        public void execute() throws Exception {
            JSONObject json = HttpUtil.getJSONObjectBody(getRequest());
            ATDataSourceResponse response = null;
            String auditSessionID;
            if ( json != null ) {
                auditSessionID = json.getString("auditSessionID");
                if ( auditSessionID != null ) {
                    response = mToDoDataSource.getDataSet(auditSessionID,getAuthParameters(),getParameters());
                } else {
                    response = new ATDataSourceResponse(false,"You must provide an audit session ID", null);
                }
            }
            if ( response == null ) {
                response = new ATDataSourceResponse(false,"Invalid Request", null);
            }
            getResponse().getOutputStream().print(response.toJSON().toString());
        }
    }

    private class StartTask extends RequestTask {
        public StartTask(AsyncContext mContext, HashMap<String, String> authParams, HashMap<String, String> params) {
            super(mContext, authParams, params);
        }
        @Override
        public void execute() throws Exception {
            ATAssessmentSessionStartResponse response;
            JSONObject json = HttpUtil.getJSONObjectBody(getRequest());
            JSONObject locationJSON = json.getJSONObject("location");
            if ( locationJSON == null ) {
                response = new ATAssessmentSessionStartResponse(false,null,"You must provide a location");
            } else {
                ATListItem location = new ATListItem(JSONUtils.getString(locationJSON, "id"));
                location.updateFromJSON(locationJSON);
                response = mToDoDataSource.beginAudit(location);
            }
            getResponse().getOutputStream().print(response.toJSON().toString());
        }
    }

    private class CancelTask extends RequestTask {
        public CancelTask(AsyncContext mContext, HashMap<String, String> authParams, HashMap<String, String> params) {
            super(mContext, authParams, params);
        }
        @Override
        public void execute() throws Exception {
            String auditSessionID = JSONUtils.getString(HttpUtil.getJSONObjectBody(getRequest()), "auditSessionID");
            ATResponse response;
            if ( auditSessionID == null ) {
                response = new ATResponse(false,"You must provide an audit session ID");
            } else {
                response = mToDoDataSource.cancelAudit(auditSessionID,getAuthParameters(),getParameters());
            }
            getResponse().getOutputStream().print(response.toJSON().toString());
        }
    }

    private class SaveTask extends RequestTask {
        public SaveTask(AsyncContext mContext, HashMap<String, String> authParams, HashMap<String, String> params) {
            super(mContext, authParams, params);
        }

        @Override
        public void execute() throws Exception {
            String auditSessionID = JSONUtils.getString(HttpUtil.getJSONObjectBody(getRequest()), "auditSessionID");
            ATResponse response;
            if ( auditSessionID == null ) {
                response = new ATResponse(false,"You must provide an audit session ID");
            } else {
                response = mToDoDataSource.saveAudit(auditSessionID, getAuthParameters(), getParameters());
            }
            getResponse().getOutputStream().print(response.toJSON().toString());
        }
    }

    private class DoneTask extends RequestTask {
        public DoneTask(AsyncContext mContext, HashMap<String, String> authParams, HashMap<String, String> params) {
            super(mContext, authParams, params);
        }

        @Override
        public void execute() throws Exception {
            String auditSessionID = JSONUtils.getString(HttpUtil.getJSONObjectBody(getRequest()), "auditSessionID");
            ATResponse response;
            if ( auditSessionID == null ) {
                response = new ATResponse(false,"You must provide an audit session ID");
            } else {
                response = mToDoDataSource.endAudit(auditSessionID, getAuthParameters(), getParameters());
            }
            getResponse().getOutputStream().print(response.toJSON().toString());
        }
    }

    private class SubmitTask extends RequestTask {
        private HttpServlet mHttpServlet;
        public SubmitTask(AsyncContext mContext, HashMap<String, String> authParams, HashMap<String, String> params, HttpServlet servlet) {
            super(mContext, authParams, params);
            mHttpServlet = servlet;
        }

        @Override
        public void execute() throws Exception {
            JSONObject dataSourceJSON;
            ATDataSetItem dataSetItem;
            JSONObject jsonObject;
            FileUpload fileUpload;
            String json = null;
            ATAttachmentUploadItem attachmentUploadItem;
            HashMap<String, ATAttachmentUploadItem> attachmentMap;
            List<FileItem> items;
            ATResponse response;
            String auditSessionID = null;


            attachmentMap = new HashMap<String, ATAttachmentUploadItem>();
            fileUpload = getServletFileUpload(mHttpServlet);

            try {
                items = fileUpload.parseRequest(getRequest());
                Iterator<FileItem> iter = items.iterator();
                while (iter.hasNext()) {
                    FileItem item = iter.next();

                    if (item.isFormField() && item.getFieldName().equalsIgnoreCase("formJSON")) {
                        json = item.getString();
                    } else if ( item.isFormField() && item.getFieldName().equalsIgnoreCase("auditSessionID") ) {
                        auditSessionID = item.getString();
                    } else if (!item.isFormField()) {
                        String fieldName = item.getFieldName();
                        String fileName = item.getName();
                        attachmentUploadItem = new ATAttachmentUploadItem(fileName,fieldName,item);
                        attachmentMap.put(fieldName,attachmentUploadItem);
                    }
                }
                if ( auditSessionID == null ) {
                    response = new ATResponse(false,"You must provide an audit session ID");
                    getResponse().getOutputStream().print(response.toJSON().toString());
                    return;
                }
                if ( json != null ) {
                    jsonObject = new JSONObject(json);
                    dataSetItem = dataSetItemForJSON(jsonObject, attachmentMap);
                    response = mToDoDataSource.updateToDoItem(auditSessionID, dataSetItem, getAuthParameters(), getURLParams(getRequest()));
                    if ( response == null ) {
                        response = new ATResponse(false, "No response was received from the handler. Please make sure you implemented updateToDoItem");
                    }
                    getResponse().getOutputStream().print(response.toJSON().toString());
                }
            } catch (FileUploadException e) {
                e.printStackTrace();
                response = new ATResponse(false,"An error occured during the file upload: " + e.getMessage());
                getResponse().getOutputStream().print(response.toJSON().toString());
            }
        }
    }
}
