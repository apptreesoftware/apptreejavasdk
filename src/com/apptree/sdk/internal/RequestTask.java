package com.apptree.sdk.internal;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * Created by Matthew Smith, AppTree Software LLC on 4/17/15.
 */
abstract class RequestTask implements Runnable {

    protected AsyncContext mAsyncContext;
    protected HashMap<String,String> mAuthParameters;

    protected HashMap<String,String> mParameters;

    /**
     * Create a request task
     * @param mContext The AsyncContext
     * @param authParams A HashMap of authorization parameters included in the request headers
     * @param params A HashMap of URL parameters
     */
    public RequestTask(AsyncContext mContext, HashMap<String, String> authParams, HashMap<String, String> params) {
        mAsyncContext = mContext;
        mAuthParameters = authParams;
    }

    /**
     *
     * @return The http servlet request from AsyncContext
     */
    protected HttpServletRequest getRequest() {
        return (HttpServletRequest) mAsyncContext.getRequest();
    }

    /**
     *
     * @return The http servlet response from AsyncContext
     */
    protected HttpServletResponse getResponse() {
        return (HttpServletResponse) mAsyncContext.getResponse();
    }

    /**
     *
     * @return The AsyncContext
     */
    protected AsyncContext getContext() {
        return mAsyncContext;
    }

    /**
     *
     * @return The AsyncContext
     */
    public AsyncContext getAsyncContext() {
        return mAsyncContext;
    }

    /**
     *
     * @return A HashMap of authorization parameters included in the request headers
     */
    public HashMap<String, String> getAuthParameters() {
        return mAuthParameters;
    }

    /**
     *
     * @return The URL parameters included in the request
     */
    public HashMap<String, String> getParameters() {
        return mParameters;
    }

    public abstract void execute() throws Exception;

    @Override
    public void run() {
        try {
            execute();
        } catch (Exception e) {
            getResponse().setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            getAsyncContext().complete();
        }
    }
}
