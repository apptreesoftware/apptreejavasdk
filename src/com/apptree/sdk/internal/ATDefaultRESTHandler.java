package com.apptree.sdk.internal;

import com.apptree.sdk.datasource.ATDefaultDataSource;
import com.apptree.sdk.datasource.response.ATDefaultDataSourceResponse;
import com.apptree.sdk.model.RESTParser;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Alexis Andreason on 2/5/15.
 */
final class ATDefaultRESTHandler extends ATRESTHandler {
    ATDefaultDataSource mFormDefaultDatasource;

    /**
     * Creates a default REST Handler with the given data source
     * @param defaultDataSource The data source to be used
     */
    public ATDefaultRESTHandler(ATDefaultDataSource defaultDataSource) {
        mFormDefaultDatasource = defaultDataSource;
    }


    /**
     * The get call prints the form defaults to the servlet resposne output stream
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap containing any authorization parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        RESTParser restParser;
        ATDefaultDataSourceResponse formDefaultDataSourceResponse;
        JSONArray formDefaultArray;
        String responseString = null;

        restParser = new RESTParser(request.getPathInfo(), RESTPath());
        formDefaultDataSourceResponse = mFormDefaultDatasource.getDefaults(authInfo, restParser.getUrlParams());
        formDefaultArray = formDefaultDataSourceResponse.getFormDefaults().toJSONArray();
        if ( formDefaultArray != null ) {
            responseString = formDefaultArray.toString();
        }
        if ( responseString != null ) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getOutputStream().print(responseString);
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public String RESTPath() {
        return mFormDefaultDatasource.dataSourceRESTPath();
    }
}
