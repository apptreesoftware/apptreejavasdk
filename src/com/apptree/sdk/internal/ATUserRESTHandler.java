package com.apptree.sdk.internal;

import com.apptree.sdk.auth.ATAuthenticationHandler;
import com.apptree.sdk.datasource.ATListDataSource;
import com.apptree.sdk.datasource.ATUserDataSource;
import com.apptree.sdk.datasource.response.ATUserInfoResponse;
import com.danisola.restify.url.RestParser;
import com.danisola.restify.url.RestParserFactory;
import com.danisola.restify.url.RestUrl;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.danisola.restify.url.types.StrVar.strVar;

/**
 * Created by Matthew Smith on 9/9/15.
 * Copyright AppTree Software, Inc.
 */
public class ATUserRESTHandler extends ATRESTHandler {

    private ATUserDataSource userDataSource;

    public ATUserDataSource getUserDataSource() {
        return userDataSource;
    }

    public void setUserDataSource(ATUserDataSource userDataSource) {
        this.userDataSource = userDataSource;
    }

    public ATUserRESTHandler(ATUserDataSource userDataSource) {
        this.userDataSource = userDataSource;
    }

    @Override
    public void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        RestParser restParser;
        RestUrl restURL;


        restParser = RestParserFactory.parser("/" + RESTPath() + "/{}/image", strVar("userID"));
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            InputStream inputStream = userDataSource.getUserImage();
            if ( inputStream != null ) {
                IOUtils.copy(inputStream, response.getOutputStream());
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
            return;
        }

        restParser = RestParserFactory.parser("/" + RESTPath() + "/userInfoKeys");
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            List<String> userKeys = userDataSource.getUserInfoKeys();
            if ( userKeys == null ) {
                userKeys = new ArrayList<String>();
            }
            JSONArray userKeysJSONArray = new JSONArray();
            for ( String key : userKeys ) {
                userKeysJSONArray.put(key);
            }
            response.getOutputStream().print(userKeysJSONArray.toString());
            return;
        }

        HashMap<String, String> urlParams = getURLParams(request);
        restParser = RestParserFactory.parser("/" + RESTPath() + "/{}", strVar("userID"));
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            ATUserInfoResponse userInfoResponse = userDataSource.getUserInfo((String) restURL.variable("userID"), authInfo, urlParams);
            if (userInfoResponse != null) {
                response.getOutputStream().print(userInfoResponse.toJSON().toString());
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
            return;
        }
    }

    @Override
    public void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {

    }

    @Override
    public void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {

    }

    @Override
    public void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {

    }

    @Override
    public void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {

    }

    @Override
    public String RESTPath() {
        return "user";
    }
}
