package com.apptree.sdk.internal;

import com.apptree.sdk.datasource.response.ATConfigurationResponse;
import com.apptree.sdk.datasource.ATDataSource;
import com.apptree.sdk.datasource.response.ATDataSourceResponse;
import com.apptree.sdk.datasource.response.ATDefaultDataSourceResponse;
import com.apptree.sdk.model.ATAttachmentUploadItem;
import com.apptree.sdk.model.HttpUtil;
import com.apptree.sdk.model.ATDataSetItem;
import com.danisola.restify.url.RestParser;
import com.danisola.restify.url.RestParserFactory;
import com.danisola.restify.url.RestUrl;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.danisola.restify.url.types.IntVar.intVar;
import static com.danisola.restify.url.types.StrVar.strVar;

/**
 * Created by Matthew Smith on 11/5/14.
 * Copyright AppTree Software, Inc.
 */
public final class ATDataRESTHandler extends ATRESTHandler {
    private ATDataSource mDataSource;

    /**
     * Constricts a RESTful handler from a given data source
     * @param dataSource The data source to be used for the handler
     */
    public ATDataRESTHandler(ATDataSource dataSource) {
        mDataSource = dataSource;
    }


    /**
     * The get handler for a data source prints a data source response to servlet response output stream;
     * prints the configuration for /describe, prints the configuration of a data source for /config, prints a
     * single data set item for /{item ID}, prints the form defaults for /formdefaults, and prints the entire
     * list of data set items if only the RESTPath() is given
     * @param servlet The servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authorization info included in
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        ATDataSourceResponse dataSourceResponse;
        ATDefaultDataSourceResponse defaultDataSourceResponse;
        ATConfigurationResponse configurationResponse;
        JSONObject dataSourceJSON;
        HashMap<String, String> urlParams;
        RestParser restParser;
        RestUrl restURL;

        urlParams = getURLParams(request);
        restParser = RestParserFactory.parser("/" + RESTPath() + "/describe");
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            configurationResponse = mDataSource.getConfiguration(authInfo, urlParams);
            response.getOutputStream().print(configurationResponse.toJSON().toString());
            return;
        }
        restParser = RestParserFactory.parser("/"+RESTPath()+"/config");
        restURL = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restURL.isValid() ) {
            configurationResponse = mDataSource.getConfiguration(authInfo, urlParams);
            response.getOutputStream().print(configurationResponse.toJSON().toString());
            return;
        }
        restParser = RestParserFactory.parser("/"+RESTPath()+"/describe");
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            configurationResponse = mDataSource.getConfiguration(authInfo, urlParams);
            response.getOutputStream().print(configurationResponse.toJSON().toString());
            return;
        }
        restParser = RestParserFactory.parser("/" + RESTPath() + "/formdefaults");
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            defaultDataSourceResponse = mDataSource.getDefaults(authInfo, urlParams);
            dataSourceJSON = defaultDataSourceResponse.toJSON();
            response.getOutputStream().print(dataSourceJSON.toString());
            return;
        }
        restParser = RestParserFactory.parser("/"+RESTPath() + "/{}",strVar("primaryKey"));
        restURL = restParser.parse(request.getPathInfo(),request.getQueryString());
        if ( restURL.isValid() ) {
            dataSourceResponse = mDataSource.getDataSetItem(authInfo, (String) restURL.variable("primaryKey"), urlParams);
            if ( dataSourceResponse.isAuthorizationError() ) {
                response.setStatus(401);
                if ( dataSourceResponse.getMessage() != null ) {
                    response.getOutputStream().print(dataSourceResponse.getMessage());
                }
                return;
            }
            dataSourceJSON = dataSourceResponse.toJSON();
            response.getOutputStream().print(dataSourceJSON.toString());
            return;
        }
        restParser = RestParserFactory.parser("/"+RESTPath());
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            int pageSize = 0;
            int pageOffset = 0;
            try {
                pageSize = Integer.parseInt(urlParams.get("pageSize"));
            } catch (NumberFormatException e) {}
            try {
                pageOffset = Integer.parseInt(urlParams.get("offset"));
            } catch (NumberFormatException e) {}

            dataSourceResponse = mDataSource.getDataSet(authInfo,pageOffset,pageSize, urlParams);
            if ( dataSourceResponse.isAuthorizationError() ) {
                response.setStatus(401);
                if ( dataSourceResponse.getMessage() != null ) {
                    response.getOutputStream().print(dataSourceResponse.getMessage());
                }
                return;
            }
            dataSourceJSON = dataSourceResponse.toJSON();
            response.getOutputStream().print(dataSourceJSON.toString());
            return;
        }

        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    /**
     * The post call for a data source prints a response to the servlet response output stream;
     * searches the data set if the path is RESTPath()/search, creates a new data set item otherwise
     * @param servlet The servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authorization parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        ATDataSourceResponse dataSourceResponse = null;
        JSONObject dataSourceJSON;
        ATDataSetItem dataSetItem;
        JSONObject jsonObject;
        HashMap<String, String> urlParams;
        FileUpload fileUpload;
        String json = null;
        ATAttachmentUploadItem attachmentUploadItem;
        HashMap<String, ATAttachmentUploadItem> attachmentMap;
        List<FileItem> items;

        RestParser restParser;
        RestUrl restURL;

        urlParams = getURLParams(request);
        restParser = RestParserFactory.parser("/" + RESTPath() + "/search");
        restURL = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restURL.isValid() ) {
            int pageSize = 0;
            int pageOffset = 0;
            try {
                pageSize = Integer.parseInt(urlParams.get("pageSize"));
            } catch (NumberFormatException e) {}
            try {
                pageOffset = Integer.parseInt(urlParams.get("offset"));
            } catch (NumberFormatException e) {}

            jsonObject = HttpUtil.getJSONObjectBody(request);
            dataSetItem = dataSetItemForJSON(jsonObject,null);
            dataSourceResponse = mDataSource.queryDataSet(dataSetItem, authInfo, pageOffset, pageSize, urlParams);

        } else {
            try {
                attachmentMap = new HashMap<String, ATAttachmentUploadItem>();
                fileUpload = getServletFileUpload(servlet);
                items = fileUpload.parseRequest(request);
                Iterator<FileItem> itemIterator = items.iterator();
                while (itemIterator.hasNext()) {
                    FileItem item = itemIterator.next();
                    if (item.isFormField() && item.getFieldName().equalsIgnoreCase("formJSON")) {
                        json = item.getString();
                    } else if (!item.isFormField()) {
                        String fieldName = item.getFieldName();
                        String fileName = item.getName();
                        attachmentUploadItem = new ATAttachmentUploadItem(fileName,fieldName,item);
                        attachmentMap.put(fieldName,attachmentUploadItem);
                    }
                }
                if ( json != null ) {
                    jsonObject = new JSONObject(json);
                    dataSetItem = dataSetItemForJSON(jsonObject,attachmentMap);
                    dataSourceResponse = mDataSource.createDataSetItem(dataSetItem, authInfo, urlParams);
                    if ( dataSourceResponse == null ) {
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        return;
                    }
                }
            } catch (FileUploadException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
        }
        if (dataSourceResponse == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        if ( dataSourceResponse.isAuthorizationError() ) {
            response.setStatus(401);
            if ( dataSourceResponse.getMessage() != null ) {
                response.getOutputStream().print(dataSourceResponse.getMessage());
            }
            return;
        }
        dataSourceJSON = dataSourceResponse.toJSON();
        if (dataSourceJSON != null) {
            response.getOutputStream().print(dataSourceJSON.toString());
        }
    }

    /**
     * The put call for a data source updates a data set item from a data source and prints a data source reponse to
     * the http servlet response output stream
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authParams A HashMap of any authentication parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authParams) throws ServletException, IOException {
        ATDataSourceResponse dataSourceResponse;
        JSONObject dataSourceJSON;
        ATDataSetItem dataSetItem;
        JSONObject jsonObject;
        FileUpload fileUpload;
        String json = null;
        ATAttachmentUploadItem attachmentUploadItem;
        HashMap<String, ATAttachmentUploadItem> attachmentMap;
        List<FileItem> items;

        attachmentMap = new HashMap<String, ATAttachmentUploadItem>();
        fileUpload = getServletFileUpload(servlet);

        try {
            items = fileUpload.parseRequest(request);
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = iter.next();

                if (item.isFormField() && item.getFieldName().equalsIgnoreCase("formJSON")) {
                    json = item.getString();
                } else if (!item.isFormField()) {
                    String fieldName = item.getFieldName();
                    String fileName = item.getName();
                    attachmentUploadItem = new ATAttachmentUploadItem(fileName,fieldName,item);
                    attachmentMap.put(fieldName,attachmentUploadItem);
                }
            }
            if ( json != null ) {
                jsonObject = new JSONObject(json);
                dataSetItem = dataSetItemForJSON(jsonObject,attachmentMap);
                dataSourceResponse = mDataSource.updateDataSetItem(dataSetItem, authParams,getURLParams(request));
                if ( dataSourceResponse == null ) {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    return;
                }
                if ( dataSourceResponse.isAuthorizationError() ) {
                    response.setStatus(401);
                    if ( dataSourceResponse.getMessage() != null ) {
                        response.getOutputStream().print(dataSourceResponse.getMessage());
                    }
                    return;
                }
                dataSourceJSON = dataSourceResponse.toJSON();
                if ( dataSourceJSON != null ) {
                    response.getOutputStream().print(dataSourceJSON.toString());
                }
            }

        } catch (FileUploadException e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * The delete call for a data source deletes a data set item and prints a data source response to the servlet
     * response output stream
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap containing any authorization parameters included in the request header
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        ATDataSourceResponse dataSourceResponse;
        JSONObject dataSourceJSON;
        ATDataSetItem dataSetItem;
        JSONObject jsonObject;

        jsonObject = HttpUtil.getJSONObjectBody(request);
        dataSetItem = dataSetItemForJSON(jsonObject, null);
        dataSourceResponse = mDataSource.deleteDataSetItem(dataSetItem, authInfo, getURLParams(request));
        if (dataSourceResponse == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        if ( dataSourceResponse.isAuthorizationError() ) {
            response.setStatus(401);
            if ( dataSourceResponse.getMessage() != null ) {
                response.getOutputStream().print(dataSourceResponse.getMessage());
            }
            return;
        }
        dataSourceJSON = dataSourceResponse.toJSON();
        if (dataSourceJSON != null) {
            response.getOutputStream().print(dataSourceJSON.toString());
        }
    }

    @Override
    public void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {

    }

    /***
     * Returns the REST path endpoint from the data source
     * @return A string that you want to use for the endpoint defined in your data source
     */
    @Override
    public String RESTPath() {
        return mDataSource.dataSourceRESTPath();
    }

}
