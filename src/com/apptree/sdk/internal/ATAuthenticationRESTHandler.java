package com.apptree.sdk.internal;

import com.apptree.sdk.auth.ATAuthenticationHandler;
import com.apptree.sdk.auth.ATLoginResponse;
import com.apptree.sdk.auth.ATLogoutResponse;
import com.apptree.sdk.auth.ATValidateTokenResponse;
import com.apptree.sdk.model.HttpUtil;
import com.apptree.sdk.util.JSONUtils;
import com.apptree.sdk.model.RESTParser;
import com.danisola.restify.url.RestParser;
import com.danisola.restify.url.RestParserFactory;
import com.danisola.restify.url.RestUrl;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by matthew on 12/26/14.
 */
final class ATAuthenticationRESTHandler extends ATRESTHandler {

    private ATAuthenticationHandler mAuthenticationHandler;
    private static final String TOKEN_HEADER_KEY = "X-Auth-Token";

    /**
     * Constructs an authentication handler with an authentication handler
     * @param authenticationHandler The authentication handler
     */
    public ATAuthenticationRESTHandler(ATAuthenticationHandler authenticationHandler) {
        mAuthenticationHandler = authenticationHandler;
    }

    @Override
    public void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        String token;
        ATValidateTokenResponse tokenResponse;
        RestParser restParser;
        RestUrl restUrl;

        if ( mAuthenticationHandler == null ) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        restParser = RestParserFactory.parser("/" + RESTPath() + "/validate");
        restUrl = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restUrl.isValid() ) {
            tokenResponse = mAuthenticationHandler.validateAuthentication(authInfo);
            response.setStatus(HttpServletResponse.SC_OK);
            response.getOutputStream().print(tokenResponse.toJSON().toString());
            return;
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        RESTParser restParser;
        JSONObject json;
        String username;
        String password;
        ATLoginResponse loginResponse;
        ATLogoutResponse logoutResponse;
        String token;

        restParser = new RESTParser(request.getPathInfo(),RESTPath());

        if ( mAuthenticationHandler == null ) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        if ( restParser.getEntity().equalsIgnoreCase("login") ) {
            json = HttpUtil.getJSONObjectBody(request);
            username = JSONUtils.getString(json,"username");
            password = JSONUtils.getString(json,"password");
            loginResponse = mAuthenticationHandler.login(username,password,json);
            response.setStatus(HttpServletResponse.SC_OK);
            response.getOutputStream().print(loginResponse.toJSON().toString());
            return;
        } else if ( restParser.getEntity().equalsIgnoreCase("logout") ) {
            token = request.getHeader(TOKEN_HEADER_KEY);
            logoutResponse = mAuthenticationHandler.logout(token);
            response.getOutputStream().print(logoutResponse.toJSON().toString());
            response.setStatus(HttpServletResponse.SC_OK);
            return;
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        if ( mAuthenticationHandler == null ) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
    }

    @Override
    public void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {

    }

    @Override
    public void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {

    }

    @Override
    public String RESTPath() {
        return "auth";
    }
}
