package com.apptree.sdk.internal;

import com.apptree.sdk.datasource.ATAttachmentDataSource;
import com.apptree.sdk.datasource.response.ATAttachmentDataSourceResponse;
import com.danisola.restify.url.RestParser;
import com.danisola.restify.url.RestParserFactory;
import com.danisola.restify.url.RestUrl;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

import static com.danisola.restify.url.types.StrVar.strVar;

/**
 * Created by alexis on 7/22/15.
 */
public class ATAttachmentRestHandler extends ATRESTHandler {

    ATAttachmentDataSource mAttachmentDataSource;

    /**
     * Constructs an attachment REST handler with a given data source
     * @param attachmentDataSource The attachment data source
     */
    public ATAttachmentRestHandler(ATAttachmentDataSource attachmentDataSource) {
        mAttachmentDataSource = attachmentDataSource;
    }

    /**
     * The GET method for an attachment data source
     * @param servlet The servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authorization parameters sent in the servlet request
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        RestParser restParser = RestParserFactory.parser("/" + RESTPath() + "/{}", strVar("primaryKey"));
        RestUrl restUrl = restParser.parse(request.getPathInfo(), request.getQueryString());
        if ( restUrl.isValid() ) {
            HashMap<String, String> urlParams;

            urlParams = getURLParams(request);
            ATAttachmentDataSourceResponse attachmentResponse = mAttachmentDataSource.getAttachment(authInfo, urlParams, (String)restUrl.variable("primaryKey"));
            if ( attachmentResponse.getAttachmentStream() != null ) {
                response.addHeader("Content-Type", attachmentResponse.getContentType());
                IOUtils.copy(attachmentResponse.getAttachmentStream(), response.getOutputStream());
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    public void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String, String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    /***
     * Returns the REST path endpoint from the data source
     * @return A string that you want to use for the endpoint defined in your data source
     */
    @Override
    public String RESTPath() {
        return mAttachmentDataSource.dataSourceRESTPath();
    }
}
