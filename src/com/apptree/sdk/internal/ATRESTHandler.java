package com.apptree.sdk.internal;

import com.apptree.sdk.model.ATAttachmentUploadItem;
import com.apptree.sdk.model.ATDataSetItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Matthew Smith on 11/5/14.
 * Copyright AppTree Software, Inc.
 */
abstract class ATRESTHandler {

    /**
     * The get call for a RESTHandler
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authentication parameters included in the request header
     * @throws ServletException
     * @throws IOException
     */
    public abstract void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException;

    /**
     * The post call for a RESTHandler
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap containing any authorization parameters from the request headers
     * @throws ServletException
     * @throws IOException
     */
    public abstract void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException;

    /**
     * The put call for a REST handler
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authentication parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    public abstract void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException;

    /**
     * The delete call for a REST handler
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authentication parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    public abstract void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response,HashMap<String,String> authInfo) throws ServletException, IOException;

    /**
     * The options call for a REST handler
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authentication parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    public abstract void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException;

    /**
     * Returns the REST path endpoint from the data source
     * @return A string that you want to use for the endpoint defined in your data source
     */
    public abstract String RESTPath();

    private static ExecutorService sExecutorService = Executors.newFixedThreadPool(50);

    public static ExecutorService getExecutor() {
        return sExecutorService;
    }

    /**
     * Gets any URL parameters from an http request
     * @param request The http request
     * @return A HashMap of the URL parameters from the request
     */
    public HashMap<String,String> getURLParams(HttpServletRequest request) {
        String paramString;
        String pathComponents[];
        String keyValueComponents[];
        String keyValuePair;
        HashMap<String,String> urlParams;

        urlParams = new HashMap<String, String>();
        if ( request.getQueryString() != null && request.getQueryString().length() > 0 ) {
            paramString = request.getQueryString();
            pathComponents = paramString.split("&");
            for ( int i = 0; i < pathComponents.length; i++ ) {
                keyValuePair = pathComponents[i];
                keyValueComponents = keyValuePair.split("=");
                if ( keyValueComponents.length == 2 ) {
                    urlParams.put(keyValueComponents[0],keyValueComponents[1]);
                }
            }
        }
        return urlParams;
    }

    /**
     * Converts the JSONObject into a data set item
     * @param jsonObject The JSONObject to convert
     * @param attachmentIDMap A HashMap of the data set items
     * @return
     * @throws JSONException
     */
    protected ATDataSetItem dataSetItemForJSON(JSONObject jsonObject,HashMap<String, ATAttachmentUploadItem> attachmentIDMap) throws JSONException {
        ATDataSetItem dataSetItem;

        dataSetItem = new ATDataSetItem();
        dataSetItem.updateFromJSON(jsonObject, attachmentIDMap);
        return dataSetItem;
    }


    /**
     *
     * @param servlet
     * @return
     */
    protected ServletFileUpload getServletFileUpload(HttpServlet servlet) {
        ServletContext servletContext;
        DiskFileItemFactory factory;

        factory = new DiskFileItemFactory();
        servletContext = servlet.getServletContext();
        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(repository);
        return new ServletFileUpload(factory);
    }

}
