package com.apptree.sdk.internal;

import com.apptree.sdk.datasource.ATListDataSource;
import com.apptree.sdk.datasource.ATSearchListDataSource;
import com.apptree.sdk.model.ATListServiceConfiguration;
import com.apptree.sdk.datasource.ATCachedListDataSource;
import com.apptree.sdk.datasource.response.ATListDataSourceResponse;
import com.apptree.sdk.model.HttpUtil;
import com.apptree.sdk.model.RESTParser;
import com.danisola.restify.url.RestParser;
import com.danisola.restify.url.RestParserFactory;
import com.danisola.restify.url.RestUrl;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Matthew Smith on 11/5/14.
 * Copyright AppTree Software, Inc.
 */
final class ATListRESTHandler extends ATRESTHandler {
    ATListDataSource mListDataSource;

    /**
     * Constructs a list REST handler for the given list data source
     * @param listDataSource The list data source
     */
    public ATListRESTHandler(ATListDataSource listDataSource) {
        mListDataSource = listDataSource;
    }

    /**
     * The get call for a list data source will print the list data source when the rest path is /describe or the
     * list items otherwise to the http servlet output stream
     * @param servlet The http servlet
     * @param request The http servlet request
     * @param response The http servlet response
     * @param authInfo A HashMap of any authorization parameters included in the request headers
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        ATListDataSourceResponse listDataSourceResponse;
        JSONArray listArray;
        RESTParser restParser;
        String responseString = null;
        ATListServiceConfiguration configuration;

        restParser = new RESTParser(request.getPathInfo(), RESTPath());
        if ( restParser.getEntity().equalsIgnoreCase("describe") ) {
            configuration = mListDataSource.getDescription();
            if ( configuration != null ) {
                responseString = configuration.toJSON().toString();
            }
        } else if ( mListDataSource instanceof ATCachedListDataSource ) {
            listDataSourceResponse = ((ATCachedListDataSource) mListDataSource).getList(authInfo,restParser.getUrlParams());
            if ( listDataSourceResponse != null ) {
                responseString = listDataSourceResponse.toJSON().toString();
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        }
        if ( responseString != null ) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getOutputStream().print(responseString);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    /**
     * The post call searches a list data set if the list data source is a search list and the path is RESTPath()/search
     * @param servlet
     * @param request
     * @param response
     * @param authInfo
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPost(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        JSONObject jsonObject;
        String searchTerm;
        boolean barcodeSearch = false;
        JSONObject context = null;
        ATListDataSourceResponse listResponse;

        RestParser restParser = RestParserFactory.parser("/" + RESTPath() +"/search");
        RestUrl restURL = restParser.parse(request.getPathInfo(),request.getQueryString());

        if ( restURL.isValid() && mListDataSource instanceof ATSearchListDataSource) {
            jsonObject = HttpUtil.getJSONObjectBody(request);
            if ( jsonObject == null ) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            try {
                searchTerm = jsonObject.getString("searchTerm");
                if ( jsonObject.has("context") ) {
                    context = jsonObject.getJSONObject("context");
                }
                if ( jsonObject.has("barcodeSearch") ) {
                    barcodeSearch = jsonObject.getBoolean("barcodeSearch");
                }
                listResponse = ((ATSearchListDataSource)mListDataSource).queryList(searchTerm,barcodeSearch,context,authInfo,getURLParams(request));
                response.getOutputStream().print(listResponse.toJSON().toString());
                response.setStatus(HttpServletResponse.SC_OK);
                return;
            } catch (Exception e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPut(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doDelete(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doOptions(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, HashMap<String,String> authInfo) throws ServletException, IOException {

    }

    /***
     * Returns the REST path endpoint from the data source
     * @return A string that you want to use for the endpoint defined in your data source
     */
    @Override
    public String RESTPath() {
        return mListDataSource.dataSourceRESTPath();
    }
}
