package com.apptree.sdk.internal;

import com.apptree.sdk.auth.ATAuthenticationHandler;
import com.apptree.sdk.datasource.*;
import com.apptree.sdk.datasource.response.ATUserInfoResponse;
import com.apptree.sdk.model.MultiReadHttpServletRequest;
import com.apptree.sdk.model.RESTParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

/**
 * Created by matthew on 11/5/14.
 */
public class ATHTTPRESTController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static HashMap<String, ATRESTHandler> sEndpointHandlerMap = new HashMap<String, ATRESTHandler>();
    private static final String ROOT_REST_PATH = "apptree";
    private static List<String> sAuthenticationKeys = new ArrayList<String>();

    /**
     * Initializes an ATHTTPRESTController, extends http servlet
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();

    }

    /**
     * Adds an authentication key
     * @param key
     */
    public static void addAuthenticationKey(String key) {
        sAuthenticationKeys.add(key);
    }

    /**
     * Returns a list of authentication keys
     * @return
     */
    public List<String> getAuthenticationKeys() {
        return sAuthenticationKeys;
    }

    /**
     * Registers a data source
     * @param dataSource The data source to be registered
     */
    public static void registerDataSource(ATDataSource dataSource) {
        ATDataRESTHandler restHandler;
        restHandler = new ATDataRESTHandler(dataSource);

        sEndpointHandlerMap.put(restHandler.RESTPath(),restHandler);
    }

    /**
     * Registers a list data source
     * @param dataSource tThe list data source to be registered
     */
    public static void registerListDataSource(ATListDataSource dataSource) {
        ATListRESTHandler restHandler;
        restHandler = new ATListRESTHandler(dataSource);

        sEndpointHandlerMap.put(restHandler.RESTPath(),restHandler);
    }

    /**
     * Registers an attachment data source
     * @param dataSource The attachment data source
     */
    public static void registerAttachmentDataSource(ATAttachmentDataSource dataSource) {
        ATAttachmentRestHandler restHandler;
        restHandler = new ATAttachmentRestHandler(dataSource);

        sEndpointHandlerMap.put(restHandler.RESTPath(), restHandler);
    }

    /**
     * Registers a to do data source
     * @param dataSource The to do data source to be registered
     */
    public static void registerToDoDataSource(ATAssessmentDataSource dataSource) {
        ATAssessmentRestHandler restHandler;
        restHandler = new ATAssessmentRestHandler(dataSource);
        sEndpointHandlerMap.put(restHandler.RESTPath(),restHandler);
    }

    /**
     * Sets the authentication handler
     * @param authenticationHandler The authentication handler to be used
     */
    public static void setAuthenticationHandler(ATAuthenticationHandler authenticationHandler) {
        ATAuthenticationRESTHandler authenticationRESTHandler;
        authenticationRESTHandler = new ATAuthenticationRESTHandler(authenticationHandler);
        sEndpointHandlerMap.put(authenticationRESTHandler.RESTPath(),authenticationRESTHandler);
    }

    public static void setUserInfoDataSource(ATUserDataSource userInfoDataSource) {
        ATUserRESTHandler handler = new ATUserRESTHandler(userInfoDataSource);
        sEndpointHandlerMap.put(handler.RESTPath(), handler);
    }

    /**
     * The method routes to the correct REST call: GET, POST, PUT, DELETE, OPTIONS
     * @param request The http servlet request
     * @param response The http servlet response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RESTParser restParser;
        ATRESTHandler restHandler;
        MultiReadHttpServletRequest multiReadRequest;
        HashMap<String,String> authInfo;

        multiReadRequest = new MultiReadHttpServletRequest(request);

        restParser = new RESTParser(request,ROOT_REST_PATH);
        restHandler = restHandlerForEndpoint(restParser.getEntity().trim());
        if ( restHandler == null ) {
            response.setStatus(404);
            return;
        }
        authInfo = getAuthenticationHeaders(request);

        if ( request.getMethod().equalsIgnoreCase("GET") ) {
            restHandler.doGet(this, multiReadRequest, response, authInfo);
        } else if ( request.getMethod().equalsIgnoreCase("POST") ) {
            restHandler.doPost(this, multiReadRequest, response, authInfo);
        } else if ( request.getMethod().equalsIgnoreCase("PUT") ) {
            restHandler.doPut(this, multiReadRequest, response, authInfo);
        } else if ( request.getMethod().equalsIgnoreCase("DELETE") ) {
            restHandler.doDelete(this, multiReadRequest, response, authInfo);
        } else if ( request.getMethod().equalsIgnoreCase("OPTION") ) {
            restHandler.doOptions(this, multiReadRequest, response, authInfo);
        }
    }

    /**
     * Registers the RESTPath() of a data source to the correct data source
     * @param endpoint The endpoint of the RESTPath()
     * @return
     */
    private ATRESTHandler restHandlerForEndpoint(String endpoint) {
        ATRESTHandler handler;
        handler = sEndpointHandlerMap.get(endpoint);
        return handler;
    }

    /**
     * Adds default headers to the http response
     * @param response The http response
     */
    private void addDefaultHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Token, X-APIKEY, X-Auth-Token, Authorization");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
        response.addHeader("Access-Control-Expose-Headers", "X-APIKEY, X-Auth-Token");
    }

    /**
     * Gets any authentication parameters included in the request headers
     * @param request
     * @return
     */
    private HashMap<String,String> getAuthenticationHeaders(HttpServletRequest request) {
        String headerName;
        HashMap<String, String> headers;

        headers = new HashMap<String, String>();
        Enumeration<String> enumeration;
        enumeration = request.getHeaderNames();
        while ( enumeration.hasMoreElements() ) {
            headerName = enumeration.nextElement();
            if ( this.getAuthenticationKeys().contains(headerName) ) {
                headers.put(headerName,request.getHeader(headerName));
            }
        }
        return headers;
    }
}
