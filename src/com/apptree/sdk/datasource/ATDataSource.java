package com.apptree.sdk.datasource;

import com.apptree.sdk.datasource.response.ATConfigurationResponse;
import com.apptree.sdk.datasource.response.ATDataSourceResponse;
import com.apptree.sdk.datasource.response.ATDefaultDataSourceResponse;
import com.apptree.sdk.model.ATDataSetItem;
import com.apptree.sdk.model.ATServiceConfigurationAttribute;

import java.util.HashMap;
import java.util.List;

/**
 * Created by matthew on 11/5/14.
 */
public abstract class ATDataSource {

    /***
     * Returns the REST path endpoint you want to use for this service. Any path you specify will result in a URL of http://{server-url}/apptree/{path}
     * @return A string that you want to use for the endpoint
     */
    public abstract String dataSourceRESTPath();

    /***
     *
     * @param authInfo A HashMap of any authentication information that came through in the request headers from the mobile client
     * @param offset The startOffset of the data the client is requesting. Will be 0 if paging is not supported.
     * @param pageSize The number or records the client is requesting
     * @param params a HashMap of the URL parameters included in the request.
     * @return The data source response that contains the list of data set items you want to return
     */
    public abstract ATDataSourceResponse getDataSet(HashMap<String,String> authInfo,int offset, int pageSize, HashMap<String,String> params);

    /***
     *
     * @param authInfo a HashMap of any authentication information that came through in the request headers from the mobile client
     * @param id The ID of the item to fetch
     * @param params a HashMap of the URL parameters included in the request
     * @return The data source response that contains the data set item with the requested ID
     */
    public abstract ATDataSourceResponse getDataSetItem(HashMap<String,String> authInfo,String id, HashMap<String,String> params);

    /**
     *
     * @param queryDataItem The data set item containing the values to be searched on
     * @param authInfo a HashMap of any authentication parameters that came through in the request headers
     * @param offset The start offset of the data the client is requesting. Will be 0 if paging is not supported
     * @param pageSize The number of records the client is requesting
     * @param params a HashMap of the URL parameters included in the request
     * @return The data source response that contains the list of data set items which meet the search criteria
     */
    public abstract ATDataSourceResponse queryDataSet(ATDataSetItem queryDataItem, HashMap<String,String> authInfo, int offset, int pageSize, HashMap<String,String> params);

    /**
     *
     * @param dataSetItem The data set item to be created
     * @param authInfo a Hashmap of any authentication parameters that came through the request headers
     * @param params a HashMap of the URL parameters included in the request
     * @return The data source response that contains the newly created data set item
     */
    public abstract ATDataSourceResponse createDataSetItem(ATDataSetItem dataSetItem, HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     *
     * @param dataSetItem The data set item to be updated
     * @param authInfo a HashMap of any authentication parameters that came from the request headers
     * @param params a Hashmap of the URL parameters included in the request
     * @return The data source response that contains the updated data set item
     */
    public abstract ATDataSourceResponse updateDataSetItem(ATDataSetItem dataSetItem, HashMap<String, String> authInfo, HashMap<String,String> params);

    /**
     *
     * @param dataSetItem The data set item to be deleted
     * @param authInfo a HashMap of any authentication parameters that came from the request headers
     * @param params a HashMap of the URL parameters included in the request
     * @return The data source response
     */
    public abstract ATDataSourceResponse deleteDataSetItem(ATDataSetItem dataSetItem, HashMap<String, String> authInfo, HashMap<String,String> params);

    /**
     *
     * @param authInfo a Hashmap of any authentication parameters that came from the request headers
     * @param params a HashMap of the URL prarameters included in the request
     * @return The configuration response which contains the available attributes for updating/displaying, creating and searching a data set
     */
    public abstract ATConfigurationResponse getConfiguration(HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     *
     * @param authInfo a HashMap of any authentication parameters that came from the request headers
     * @param params a HashMap of the URL parameters included in the request
     * @return The default data source response which includes any default values for data set items
     */
    public abstract ATDefaultDataSourceResponse getDefaults(HashMap<String, String> authInfo, HashMap<String, String> params);
}
