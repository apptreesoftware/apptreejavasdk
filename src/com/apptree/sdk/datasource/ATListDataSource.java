package com.apptree.sdk.datasource;

import com.apptree.sdk.model.ATCacheListServiceConfiguration;
import com.apptree.sdk.model.ATListServiceConfiguration;

/**
 * Created by Matthew Smith, AppTree Software LLC on 3/31/15.
 */
public abstract class ATListDataSource {

    /**
     *
     * @return A list service configuration containing the attributes of a list
     */
    public abstract ATListServiceConfiguration getDescription();

    /***
     * Returns the REST path endpoint you want to use for this service. Any path you specify will result in a URL of http://{server-url}/apptree/{path}
     * @return A string that you want to use for the endpoint
     */
    public abstract String dataSourceRESTPath();
}
