package com.apptree.sdk.datasource;

import com.apptree.sdk.datasource.response.ATUserInfoResponse;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Matthew Smith on 9/9/15.
 * Copyright AppTree Software, Inc.
 */
public abstract class ATUserDataSource {

    /**
     * Get the list of available user attributes that you want to provide
     * @return List of Strings representing the attributes you want to provide. These values will be available in the builder.
     */
    public abstract List<String> getUserInfoKeys();

    /**
     * Get the user information given a userID. This is your opportunity to return user attributes like email, phone number etc.
     * @param userID the user ID that the information is being requested for
     * @param authInfo authentication information, use this to validate the request has access to this user info
     * @param urlParams any additional urlParams
     * @return a ATUserInfoResponse containing information about the requested user
     */

    public abstract ATUserInfoResponse getUserInfo(String userID, HashMap<String,String> authInfo, HashMap<String,String> urlParams);

    /***
     * Get the users 'avatar' image
     * @return an InputStream for the users image data. JPG an PNG are supported by the client.
     */
    public abstract InputStream getUserImage();
}
