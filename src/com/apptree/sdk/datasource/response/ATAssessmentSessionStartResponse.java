package com.apptree.sdk.datasource.response;

import com.apptree.sdk.datasource.ATResponse;
import org.json.JSONObject;

/**
 * Created by Matthew Smith, AppTree Software LLC on 4/17/15.
 */
public class ATAssessmentSessionStartResponse extends ATResponse {
    private String mAuditSessionID;

    /**
     * Begins an assessment session
     * @param success A boolean indicating the succes of starting an assessment
     * @param auditSessionID An string ID for the session
     * @param message A string message about the call
     */
    public ATAssessmentSessionStartResponse(boolean success, String auditSessionID, String message) {
        super(success,message);
        mAuditSessionID = auditSessionID;
    }

    /**
     *
     * @return A String ID for this session
     */
    public String getAuditSessionID() {
        return mAuditSessionID;
    }

    /**
     *
     * @return A JSONObject of the assessment session start response
     */
    public JSONObject toJSON() {
        JSONObject jsonObject = super.toJSON();
        jsonObject.putOpt("auditSessionID", mAuditSessionID);
        return jsonObject;
    }
}
