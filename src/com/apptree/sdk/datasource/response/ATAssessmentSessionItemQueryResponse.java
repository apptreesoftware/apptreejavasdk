package com.apptree.sdk.datasource.response;

import com.apptree.sdk.datasource.ATResponse;
import com.apptree.sdk.model.ATDataSetItem;
import org.json.JSONObject;

/**
 * Created by Matthew Smith, AppTree Software LLC on 4/17/15.
 */
public class ATAssessmentSessionItemQueryResponse extends ATResponse {
    ATDataSetItem mDataSetItem;

    /**
     *
     * @param success A boolean indicating the success of the call
     * @param message A message about the call
     * @param dataSetItem An assessment data set item
     */
    public ATAssessmentSessionItemQueryResponse(boolean success, String message, ATDataSetItem dataSetItem) {
        super(success,message);
        mDataSetItem = dataSetItem;
    }

    /**
     *
     * @return An assessment data set item
     */
    public ATDataSetItem getDataSetItem() {
        return mDataSetItem;
    }

    /**
     *
     * @return A JSONObject for the assessment session item query response
     */
    public JSONObject toJSON() {
        if ( mDataSetItem != null ) {
            setRecord(mDataSetItem.toJSON());
        }
        return super.toJSON();
    }

}
