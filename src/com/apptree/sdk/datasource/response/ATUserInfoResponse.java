package com.apptree.sdk.datasource.response;

import com.apptree.sdk.datasource.ATResponse;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Matthew Smith on 9/9/15.
 * Copyright AppTree Software, Inc.
 */
public class ATUserInfoResponse extends ATResponse {

    private String userID;
    private List<Integer> roleIDs;

    private HashMap<String,String> userInfo = new HashMap<String, String>();

    public ATUserInfoResponse(boolean success, String message, boolean authorizationError) {
        super(success, message, authorizationError);
    }

    public ATUserInfoResponse(boolean success, String message) {
        super(success, message);
    }

    public static class Builder {
        String userID;
        String message = "";
        boolean success = false;
        boolean authorizationError = false;
        private List<Integer> roleIDs;
        private HashMap<String,String> userInfo = new HashMap<String, String>();

        public Builder() {

        }

        public Builder withSuccess(String userID) {
            this.userID = userID;
            this.success = true;
            return this;
        }

        public Builder withAuthorizationError(String message) {
            this.authorizationError = true;
            this.message = message;
            return this;
        }

        public Builder withFailure(String failureMessage) {
            this.success = false;
            this.message = failureMessage;
            return this;
        }

        public Builder withRoleIDs(List<Integer> roleIDs) {
            this.roleIDs = roleIDs;
            return this;
        }

        public Builder withUserInfoValue(String key, String value) {
            if ( key != null && value != null ) {
                userInfo.put(key,value);
            }
            return this;
        }

        public Builder withUserInfoValues(HashMap<String,String> userInfoValues) {
            userInfo.putAll(userInfoValues);
            return this;
        }

        public ATUserInfoResponse build() {
            ATUserInfoResponse response = new ATUserInfoResponse(success, message, authorizationError);
            response.userInfo = this.userInfo;
            response.roleIDs = roleIDs;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", userID);
            if ( roleIDs != null ) {
                JSONArray roleJSONArray = new JSONArray();
                for ( Integer roleID : roleIDs ) {
                    roleJSONArray.put(roleID);
                }
                jsonObject.putOpt("roleIDs",roleIDs);
            }
            JSONObject additionalUserInfo = new JSONObject();
            if ( userInfo != null ) {
                for ( String key : userInfo.keySet() ) {
                    additionalUserInfo.put(key,userInfo.get(key));
                }
            }
            jsonObject.put("additionalUserInfo",additionalUserInfo);
            response.setRecord(jsonObject);
            return response;
        }
    }
}
