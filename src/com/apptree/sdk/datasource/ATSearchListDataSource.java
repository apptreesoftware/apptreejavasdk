package com.apptree.sdk.datasource;

import com.apptree.sdk.datasource.response.ATListDataSourceResponse;
import com.apptree.sdk.model.ATSearchListServiceConfiguration;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Matthew Smith, AppTree Software LLC on 3/31/15.
 */
public abstract class ATSearchListDataSource extends ATListDataSource {

    /**
     *
     * @param queryText A String to be searched for in the list
     * @param barcodeSearch A boolean indicating if the list is searching a barcode
     * @param searchContext A JSONObject containing search parameters
     * @param authInfo A HashMap of any authentication parameters included in the request headers
     * @param params A HashMap of the URL parameters included in the request
     * @return A list data source response containing list items that satisfy the search oarameters
     */
    public abstract ATListDataSourceResponse queryList(String queryText,boolean barcodeSearch, JSONObject searchContext, HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     *
     * @return A search list service configuration containing the attributes of the list
     */
    public abstract ATSearchListServiceConfiguration getDescription();

}
