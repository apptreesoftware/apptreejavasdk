package com.apptree.sdk.datasource;
import com.apptree.sdk.datasource.response.*;
import com.apptree.sdk.model.ATDataSetItem;
import com.apptree.sdk.model.ATListItem;
import com.apptree.sdk.model.ATAssessmentConfiguration;

import java.util.HashMap;

/**
 * Created by Matthew Smith, AppTree Software LLC on 4/17/15.
 */
public abstract class ATAssessmentDataSource {
    /**
     * Returns the RESTFul path to this web service. This path will automatically be prefixed with /audit for you.
     * <p>
     * For example, if you specify a dataSourceRestPath() of 'store' the RESTFul path with be http://{server_url}/apptree/audit/store
     *
     * @return a string that will be used as the last part of the REST Path
     */
    public abstract String dataSourceRESTPath();

    /**
     * Returns the list of all the items that need to be audited for that location
     * @param sessionID The session ID created from calling beginAudit
     * @param authInfo HashMap contains any authentication information passed in by the mobile client you need to validate authentication to this resource
     * @param params HashMap any parameters that have been passed in via the URL. This is typically not used but can be in situations where you want to reuse this data source and change its behaviour by passing in parameters
     * @return ATDataSourceResponse the data set of items for the user to audit
     */

    public abstract ATDataSourceResponse getDataSet(String sessionID, HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     * Starts an audit given a location
     * @param listItem ATListItem a list item representation of your location from the getLocationList
     * @return ATAuditSessionStartResponse the response that contains the auditSessionID of the newly created audit session
     */

    public abstract ATAssessmentSessionStartResponse beginAudit(ATListItem listItem);

    /**
     * Save the current state of the audit. This is used when a user wants to save this audit and complete it at another time
     * @param auditSessionID The sessionID of the audit they want to save
     * @param authInfo HashMap contains any authentication information passed in by the mobile client you need to validate authentication to this resource
     * @param params HashMap any parameters that have been passed in via the URL. This is typically not used but can be in situations where you want to reuse this data source and change its behaviour by passing in parameters
     * @return ATResponse a response that returns whether the audit was successfully saved and any message that should be presented to the user
     */
    public abstract ATResponse saveAudit(String auditSessionID, HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     * End the audit with a given ID
     * @param auditSessionID the ID of the audit that the user wants to end
     * @param authInfo HashMap contains any authentication information passed in by the mobile client you need to validate authentication to this resource
     * @param params HashMap any parameters that have been passed in via the URL. This is typically not used but can be in situations where you want to reuse this data source and change its behaviour by passing in parameters
     * @return ATAuditSessionEndResponse a response that returns whether the audit was successfully ended and any message that should be presented to the user
     */

    public abstract ATResponse endAudit(String auditSessionID, HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     * cancel the currentAudit with the given ID
     * @param auditSessionID the ID of the audit that the user wants to cancel
     * @param authInfo HashMap contains any authentication information passed in by the mobile client you need to validate authentication to this resource
     * @param params HashMap any parameters that have been passed in via the URL. This is typically not used but can be in situations where you want to reuse this data source and change its behaviour by passing in parameters
     * @return ATResponse a response that returns whether the audit was successfully canceled and any message that should be presented to the user
     */
    public abstract ATResponse cancelAudit(String auditSessionID, HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     * When an item is found in a location that should not be there, the mobile application will make a query for that item. You can optionally return a dataSetItem that represents that item. The application can be configured to let the user take action on that item. For example, if a peice of equipment is in a store that should not be there, the mobile app will ask you to search for that item and if found, will let the user decide if they want to "move" it into the location they are currently in
     * @param auditSessionID The ID of the session
     * @param searchText The text of the scanned in item. This is scanned in from the mobile app as a barcode.
     * @param authInfo HashMap contains any authentication information passed in by the mobile client you need to validate authentication to this resource
     * @param params HashMap any parameters that have been passed in via the URL. This is typically not used but can be in situations where you want to reuse this data source and change its behaviour by passing in parameters
     * @return
     */
    public abstract ATAssessmentSessionItemQueryResponse searchForExternalDataSetItem(String auditSessionID, String searchText, HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     *
     * @param auditSessionID The ID of the session
     * @param dataSetItem The dataSetItem that the user wants to relocate
     * @param location The location the user wants to relocate to
     * @param authInfo HashMap contains any authentication information passed in by the mobile client you need to validate authentication to this resource
     * @param params HashMap any parameters that have been passed in via the URL. This is typically not used but can be in situations where you want to reuse this data source and change its behaviour by passing in parameters
     * @return an action response providing user the success status and a message to be displayed
     */

    public abstract ATResponse relocateToDoItem(String auditSessionID, ATDataSetItem dataSetItem, ATListItem location,HashMap<String,String> authInfo, HashMap<String,String> params);

    /**
     * Update a to do item. You must implement this method to handle a user submitting a form to change any data on a to do item.
     * @param auditSessionID The ID of the session
     * @param dataSetItem The to do item to be updated
     * @param authInfo HashMap contains any authentication information passed in by the mobile client you need to validate authentication to this resource
     * @param params HashMap any parameters that have been passed in via the URL. This is typically not used but can be in situations where you want to reuse this data source and change its behaviour by passing in parameters
     * @return A dataSource response containing the success of the update along with the newly updated data set item.
     */
    public abstract ATDataSourceResponse updateToDoItem(String auditSessionID, ATDataSetItem dataSetItem, HashMap<String,String> authInfo, HashMap<String,String>params);

    /**
     * Returns a configuration for this webservice
     * @return a to do configuration
     */
    public abstract ATConfigurationResponse getConfiguration();
}
