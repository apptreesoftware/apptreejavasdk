package com.apptree.sdk.datasource;

import com.apptree.sdk.datasource.response.ATListDataSourceResponse;
import com.apptree.sdk.model.ATCacheListServiceConfiguration;

import java.util.HashMap;

/**
 * Created by Matthew Smith on 11/5/14.
 * Copyright AppTree Software, Inc.
 */
public abstract class ATCachedListDataSource extends ATListDataSource {

    /**
     *
     * @param authInfo A HashMap of any authentication parameters that were included in the request headers
     * @param params A HashMap of the URL parameters included in the request
     * @return A list data source response which contains the list items
     */
    public abstract ATListDataSourceResponse getList(HashMap<String, String> authInfo, HashMap<String,String> params);

    /**
     * This returns the configuration for a list
     * @return
     */
    public abstract ATCacheListServiceConfiguration getDescription();
}
