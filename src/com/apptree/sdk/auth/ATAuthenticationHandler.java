package com.apptree.sdk.auth;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by matthew on 12/26/14.
 */
public abstract class ATAuthenticationHandler {

    /**
     * Logs in a user
     * @param username
     * @param password
     * @param json
     * @return A login response which includes a token
     */
    public abstract ATLoginResponse login(String username, String password, JSONObject json);

    /**
     * Logs out a user
     * @param token The token to be logged out
     * @return A logout response
     */
    public abstract ATLogoutResponse logout(String token);

    /**
     * Checks the validity of a token
     * @param authInfo A HashMap of any authorization parameters sent with the request
     * @return A validate token response indicating whether the token is valid
     */
    public abstract ATValidateTokenResponse validateAuthentication(HashMap<String,String>authInfo);
}
