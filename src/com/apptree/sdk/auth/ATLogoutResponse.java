package com.apptree.sdk.auth;

import org.json.JSONObject;

/**
 * Created by matthew on 12/26/14.
 */
public class ATLogoutResponse {
    boolean mSuccess;
    String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public ATLogoutResponse(boolean success, String message) {
        mSuccess = success;
        mMessage = message;
    }

    public JSONObject toJSON() {
        JSONObject jsonObject;

        jsonObject = new JSONObject();
        jsonObject.put("success",mSuccess);
        jsonObject.put("message", mMessage);
        return jsonObject;
    }
}
