package com.apptree.sdk.model;

/**
 * Created by Matthew Smith, AppTree Software LLC on 3/31/15.
 */
public class ATSearchListServiceConfiguration extends ATListServiceConfiguration {

    /**
     * Creates a list service configuration with the type set to search
     */
    public ATSearchListServiceConfiguration() {
        super();
        mListType = ListType.Search;
    }
}
