package com.apptree.sdk.model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class RESTParser {
 
    private String id;
    private String entity;
	private HashMap<String,String> urlParams;

	public RESTParser(HttpServletRequest request, String basePath) throws ServletException {
		String components[];
		String paramString;
		String pathComponents[];
		String keyValueComponents[];
		String keyValuePair;
		String pathInfo = null;

		pathInfo = request.getPathInfo();
		if ( basePath != null && !basePath.startsWith("/") ) {
			basePath = "/" + basePath;
		}

		if ( basePath != null && request.getPathInfo().startsWith(basePath) ) {
			pathInfo = pathInfo.substring(basePath.length());
		}
		if ( pathInfo.startsWith("/") ) {
			pathInfo = pathInfo.substring(1);
		}
		components = pathInfo.split("/");
		if ( components.length >= 2 ) {
			entity = components[0];
			id = components[1];
		} else if ( components.length == 1 ) {
			entity = components[0];
		} else {
			throw new ServletException("Invalid URI");
		}
		urlParams = new HashMap<String, String>();
		if ( id != null ) {
			urlParams.put("id",id);
		}
		if ( request.getQueryString() != null && request.getQueryString().length() > 0 ) {
			paramString = request.getQueryString();
			pathComponents = paramString.split("&");
			for ( int i = 0; i < pathComponents.length; i++ ) {
				keyValuePair = pathComponents[0];
				keyValueComponents = keyValuePair.split("=");
				if ( keyValueComponents.length == 2 ) {
					urlParams.put(keyValueComponents[0],keyValueComponents[1]);
				}
			}
		}
	}


    public RESTParser(String pathInfo, String basePath) throws ServletException {
    	String components[];
		String paramString;
		String pathComponents[];
		String keyValueComponents[];
		String keyValuePair;
    	
    	if ( basePath != null && !basePath.startsWith("/") ) {
    		basePath = "/" + basePath;
    	}
    	
    	if ( basePath != null && pathInfo.startsWith(basePath) ) {
    		pathInfo = pathInfo.substring(basePath.length());
    	}
    	if ( pathInfo.startsWith("/") ) {
    		pathInfo = pathInfo.substring(1);
    	}
    	components = pathInfo.split("/");
    	if ( components.length >= 2 ) {
    		entity = components[0];
    		id = components[1];
    	} else if ( components.length == 1 ) {
    		entity = components[0];
    	} else {
    		throw new ServletException("Invalid URI");
    	}
		urlParams = new HashMap<String, String>();
		if ( id != null ) {
			urlParams.put("id",id);
		}
		if ( pathInfo.contains("?") ) {
			paramString = pathInfo.substring(pathInfo.indexOf("?"));
			pathComponents = paramString.split("&");
			for ( int i = 0; i < pathComponents.length; i++ ) {
				keyValuePair = pathComponents[0];
				keyValueComponents = keyValuePair.split("=");
				if ( keyValueComponents.length == 2 ) {
					urlParams.put(keyValueComponents[0],keyValueComponents[1]);
				}
			}
		}

    }
 
    public String getId() {
      return id;
    }
 
    public void setId(String id) {
      this.id = id;
    }
    
    public String getEntity() {
    	return entity;
    }
    
    public void setEntity(String entity) {
    	this.entity = entity;
    }

	public HashMap<String,String> getUrlParams() {
		return urlParams;
	}
}
