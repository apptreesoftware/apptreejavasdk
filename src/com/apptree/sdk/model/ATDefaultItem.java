package com.apptree.sdk.model;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by alexis on 2/5/15.
 */
public class ATDefaultItem {
    private String value;
    private int dataAttributeIndex;

    /**
     * Creates a default item
     * @param newValue The string value of the item
     * @param newIndex The index of the item
     */
    public ATDefaultItem(String newValue, int newIndex) {
        this.value = newValue;
        this.dataAttributeIndex = newIndex;
    }

    /**
     * Converts the default item to a json object
     * @return a json object of the default item
     */
    public JSONObject toJSON() {
        JSONObject jsonObject;

        jsonObject = new JSONObject();
        jsonObject.put("value", this.value);
        jsonObject.put("data_attribute_index", this.dataAttributeIndex);

        return jsonObject;
    }
}
