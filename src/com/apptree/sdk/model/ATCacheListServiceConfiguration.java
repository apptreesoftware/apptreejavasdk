package com.apptree.sdk.model;

/**
 * Created by Matthew Smith, AppTree Software LLC on 3/31/15.
 */
public class ATCacheListServiceConfiguration extends ATListServiceConfiguration {

    /**
     * Creates a list service configuration with the type cache
     */
    public ATCacheListServiceConfiguration() {
        super();
        mListType = ListType.Cache;
    }
}
