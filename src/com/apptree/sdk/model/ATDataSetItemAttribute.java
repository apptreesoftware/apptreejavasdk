package com.apptree.sdk.model;

import com.apptree.sdk.util.JSONUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew Smith on 11/5/14.
 * Copyright AppTree Software, Inc.
 */
public class ATDataSetItemAttribute {
    private String mValue;
    private List<ATDataSetItem> mDataSetItems;

    /**
     * Creates an int data set item attribute
     * @param intValue
     */
    public ATDataSetItemAttribute(int intValue) {
        mValue = "" + intValue;
    }

    /**
     * Creates a boolean data set item attribute
     * @param boolValue
     */
    public ATDataSetItemAttribute(boolean boolValue){
        mValue = "" + boolValue;
    }

    /**
     * Creates a double data set item attribute
     * @param doubleValue
     */
    public ATDataSetItemAttribute(double doubleValue) {
        mValue = "" + doubleValue;
    }

    /**
     * Creates a string data set item attribute
     * @param stringValue
     */
    public ATDataSetItemAttribute(String stringValue) {
        mValue = stringValue;
    }

    /**
     * Adds a data set item to the data set item list
     * @param dataSetItem The data set item to add
     */
    public ATDataSetItemAttribute(ATDataSetItem dataSetItem) {
        mDataSetItems = new ArrayList<ATDataSetItem>(1);
        mDataSetItems.add(dataSetItem);
    }

    /**
     * Sets the list of data set item attributes
     * @param dataSetItems The list of data set item attributes
     */
    public ATDataSetItemAttribute(List<ATDataSetItem>dataSetItems) {
        mDataSetItems = dataSetItems;
    }

    public ATDataSetItemAttribute(ATListItem listItem) {
        if (listItem != null) {
            mValue = listItem.toJSON().toString();
        }
    }

    /**
     * Gets the integer value of attribute
     * @return
     */
    public int getIntValue() {
        return Integer.parseInt(mValue);
    }

    /**
     * Gets the double value of attribute
     * @return
     */
    public double getDoubleValue() {
        return Double.parseDouble(mValue);
    }

    /**
     * Gets the string value of attribute
     * @return
     */
    public String getStringValue() {
        return mValue;
    }

    /**
     * Gets the boolean value of attribute
     * @return
     */
    public boolean getBooleanValue(){
        if ( mValue != null && (mValue.equalsIgnoreCase("true") || mValue.equalsIgnoreCase("Y")) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the list of data set item attributes
     * @return
     */
    public List<ATDataSetItem> getDataSetItems() {
        return mDataSetItems;
    }

    public ATListItem getListItem() {
        JSONObject json;
        ATListItem listItem;
        if ( mValue != null ) {
            if (JSONUtils.isJson(mValue) ) {
                json = new JSONObject(mValue);
                listItem = new ATListItem(json);
                return listItem;
            }
        }
        return null;
    }

    /**
     * Converts the data set item attribute to a json object
     * @param primaryKeyRequired A boolean indicating whether the primary key is required
     * @return a sjon object
     * @throws ATInvalidPrimaryKeyException
     */
    public Object getJSONValue(boolean primaryKeyRequired) throws ATInvalidPrimaryKeyException {
        if ( mValue != null ) {
            return mValue;
        } else if ( mDataSetItems != null ) {
            return getDataSetItemsJSON(primaryKeyRequired);
        }
        return null;
    }

    /**
     * Converts the list of data set item attributes to a json object
     * @param primaryKeyRequired A boolean indicating whether or not the primary key is required
     * @return json object of the data set items list
     * @throws ATInvalidPrimaryKeyException
     */
    private JSONArray getDataSetItemsJSON(boolean primaryKeyRequired) throws ATInvalidPrimaryKeyException {
        JSONArray jsonArray = new JSONArray();

        for ( ATDataSetItem dataSetItem : mDataSetItems ) {
            if ( primaryKeyRequired ) {
                jsonArray.put(dataSetItem.toJSONWithPrimaryKey());
            } else {
                jsonArray.put(dataSetItem.toJSON());
            }
        }
        return jsonArray;
    }



}
