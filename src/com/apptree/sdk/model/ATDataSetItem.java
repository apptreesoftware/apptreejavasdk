package com.apptree.sdk.model;

import com.apptree.sdk.util.JSONUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Matthew Smith on 11/5/14.
 * Copyright AppTree Software, Inc.
 */


public class ATDataSetItem {

    public static final DateTimeFormatter AppTreeDateFormat = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(DateTimeZone.forID("Etc/GMT"));
    public static final DateTimeFormatter AppTreeDateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.forID("Etc/GMT"));

    HashMap<Integer, ATDataSetItemAttribute> mAttributeMap;
    String mPrimaryKey;
    String mClientKey;
    int mMaxAttributeIndex = -1;
    CRUDStatus mCRUDStatus = CRUDStatus.Read;
    AssessmentStatus mAssessmentStatus = AssessmentStatus.None;

    public enum Type {
        Record("RECORD"),
        Attachment("ATTACHMENT");

        private final String stringValue;

        /**
         * The type of data set item
         * @param text The name of the data set type, either RECORD or ATTACHMENT
         */
        private Type(final String text) {
            this.stringValue = text;
        }

        /**
         * Gets the type from the name
         * @param typeString The name of the type
         * @return
         */
        public static Type fromString(String typeString) {
            typeString = typeString.toUpperCase();
            if ( typeString.equals(Record.stringValue) ) {
                return Record;
            } else if ( typeString.equals(Attachment.stringValue) ) {
                return Attachment;
            }
            return Record;
        }
    }

    public enum CRUDStatus {
        None("NONE"),
        Create("CREATE"),
        Update("UPDATE"),
        Delete("DELETE"),
        Read("READ");

        private final String stringValue;

        /**
         * Sets the CRUD status of a data set item
         * @param text
         */
        private CRUDStatus(final String text) {
            this.stringValue = text;
        }

        @Override
        public String toString() {
            return stringValue;
        }

        /**
         * Sets the CRUD type from a String
         * @param crudString The CRUD type string
         * @return
         */
        public static CRUDStatus fromString(String crudString) {
            crudString = crudString.toUpperCase();
            if ( crudString.equals(Create.toString()) ) {
                return Create;
            } else if ( crudString.equals(Update.toString()) ) {
                return Update;
            } else if ( crudString.equals(Delete.toString()) ) {
                return Delete;
            } else if ( crudString.equals(Read.toString()) ) {
                return Read;
            }
            return None;
        }
    }

    public enum AssessmentStatus {
        None("NONE"),
        Accepted("ACCEPTED"),
        Skipped("SKIPPED"),
        Exception("EXCEPTION");

        private final String stringValue;
        private AssessmentStatus(final String text) {
            this.stringValue = text;
        }

        @Override
        public String toString() {
            return stringValue;
        }

        /**
         * Assessment status from name
         * @param string The name of the assessment status
         * @return
         */
        public static AssessmentStatus fromString(String string) {
            string = string.toUpperCase();
            if ( string.equalsIgnoreCase(None.toString())) {
                return None;
            } else if ( string.equalsIgnoreCase(Accepted.toString()) ) {
                return Accepted;
            } else if ( string.equalsIgnoreCase(Skipped.toString()) ) {
                return Skipped;
            } else if ( string.equalsIgnoreCase(Exception.toString()) ) {
                return Exception;
            }
            return None;
        }
    }

    /**
     * Creates a data set item
     */
    public ATDataSetItem() {
        mAttributeMap = new HashMap<Integer, ATDataSetItemAttribute>();
    }

    /**
     * Sets the primary key
     * @param primaryKey The String primary key
     */
    public void setPrimaryKey(String primaryKey) {
        mPrimaryKey = primaryKey;
    }

    /**
     * Gets the primary key
     * @return
     */
    public String getPrimaryKey() {
        return mPrimaryKey;
    }

    /**
     * Gets the item type
     * @return
     */
    public Type getItemType() {
        return Type.Record;
    }

    /**
     * Sets the CRUD status
     * @param status The status of the data set item
     */
    public void setCRUDStatus(CRUDStatus status) {
        mCRUDStatus = status;
    }

    /**
     * Gets the CRUD status
     * @return
     */
    public CRUDStatus getCRUDStatus() {
        return mCRUDStatus;
    }

    /**
     * Gets the client key
     * @return
     */
    public String getClientKey() {
        return mClientKey;
    }

    /**
     * Sets the client key
     * @param clientKey
     */
    public void setClientKey(String clientKey) {
        mClientKey = clientKey;
    }

    /**
     * Gets the assessment status
     * @return
     */
    public AssessmentStatus getAssessmentStatus() {
        return mAssessmentStatus;
    }

    /**
     * Sets the assessment status
     * @param assessmentStatus
     */
    public void setAssessmentStatus(AssessmentStatus assessmentStatus) {
        mAssessmentStatus = assessmentStatus;
    }

    /**
     * Returns a color at the specified index of the attribute map
     * @param attributeIndex The index of the attribute to get
     * @return Color
     */
    public Color getColorAttributeAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;
        List<String> colorList;
        Color colorAttribute = null;

        attribute = mAttributeMap.get(attributeIndex);
        if ( attribute != null ) {
            if ( attribute.getStringValue() != null && !attribute.getStringValue().isEmpty() ) {
                colorList = Arrays.asList(attribute.getStringValue().split("\\s*,\\s*"));
                colorAttribute = new Color(Integer.parseInt(colorList.get(0)), Integer.parseInt(colorList.get(1)), Integer.parseInt(colorList.get(2)));
            }
        }
        return colorAttribute;
    }

    /**
     * Gets an int at the specified index of the attribute map
     * @param attributeIndex The index of the attribute to get
     * @return int
     */
    public int getIntAttributeAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;
        int intAttribute;

        attribute = mAttributeMap.get(attributeIndex);
        if ( attribute != null ) {
            try {
                if ( attribute.getStringValue() != null ) {
                    intAttribute = Integer.parseInt(attribute.getStringValue());
                    setValueForAttribute(intAttribute, attributeIndex);
                }
                return attribute.getIntValue();
            } catch (NumberFormatException e) {
                return -1;
            }
        }
        return 0;
    }

    /**
     * Gets a double at the specified index of the attribute map
     * @param attributeIndex The index of the attribute
     * @return double
     */
    public double getDoubleAttributeAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;
        double doubleAttribute;

        attribute = mAttributeMap.get(attributeIndex);
        if ( attribute != null ) {
            if ( attribute.getStringValue() != null ) {
                doubleAttribute = Double.parseDouble(attribute.getStringValue());
                setValueForAttribute(doubleAttribute, attributeIndex);
            }
            return attribute.getDoubleValue();
        }
        return 0;
    }

    /**
     * Gets a list item at the specified index of the attribute map
     * @param attributeIndex The index of the attribute to get
     * @return list item
     */
    public ATListItem getListItemAttributeAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;

        attribute = mAttributeMap.get(attributeIndex);
        if (attribute != null) {
            return attribute.getListItem();
        }
        return null;
    }

    /**
     * Gets a string attriubte at the specified index of the attribute map
     * @param attributeIndex The index to get the attribute at
     * @return string
     */
    public String getStringAttributeAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;
        attribute = mAttributeMap.get(attributeIndex);
        if ( attribute != null ) {
            return attribute.getStringValue();
        }
        return null;
    }

    /**
     * Gets a date attribute at the specified index of the attribute map
     * @param attributeIndex The index to get the attribute at
     * @return date
     */
    public DateTime getDateAttributeAtIndex(int attributeIndex) {
        String dateString = getStringAttributeAtIndex(attributeIndex);
        if ( dateString != null ) {
            return AppTreeDateFormat.parseDateTime(dateString);
        }
        return null;
    }

    /**
     * Gets a date time attribute at the specified index of the attribute map
     * @param attributeIndex The index to get the attribute at
     * @return date time
     */
    public DateTime getDateTimeAttributeAtIndex(int attributeIndex) {
        String dateString = getStringAttributeAtIndex(attributeIndex);
        if ( dateString != null ) {
            return AppTreeDateTimeFormat.parseDateTime(dateString);
        }
        return null;
    }

    /**
     * Gets a boolean attribute at the specified index of the attribute map
     * @param attributeIndex The index to get the attribute at
     * @return boolean
     */
    public boolean getBooleanAttributeAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;
        attribute = mAttributeMap.get(attributeIndex);
        return attribute.getBooleanValue();
    }

    /**
     * Gets the list of data set item attribute at the specified index of the attribute map
     * @param attributeIndex The index to get the attribute at
     * @return list of data set items
     */
    public List<ATDataSetItem> getDataSetItemsAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;

        attribute = mAttributeMap.get(attributeIndex);
        if ( attribute != null ) {
            return attribute.getDataSetItems();
        }
        return null;
    }

    /**
     * Gets a data set item attribute at the specified index of the attribute map
     * @param attributeIndex The index to get the attribute at
     * @return data set item
     */
    public ATDataSetItem getDataSetItemAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;
        List<ATDataSetItem> dataSetItems;

        attribute = mAttributeMap.get(attributeIndex);
        if ( attribute != null ) {
            dataSetItems = attribute.getDataSetItems();
            if ( dataSetItems != null && dataSetItems.size() == 1 ) {
                return dataSetItems.get(0);
            }
        }
        return null;
    }

    /**
     * Gets a list of attachment item attribute at the specified index of the attribute map
     * @param attributeIndex The index to get the attribute at
     * @return list of attachment items
     */
    public List<ATDataSetAttachmentItem> getAttachmentItemsAtIndex(int attributeIndex) {
        ATDataSetItemAttribute attribute;
        ArrayList<ATDataSetAttachmentItem> attachmentItems = new ArrayList<ATDataSetAttachmentItem>();

        attribute = mAttributeMap.get(attributeIndex);
        if ( attribute != null ) {
            List<ATDataSetItem> items = attribute.getDataSetItems();
            if ( items != null ) {
                for ( ATDataSetItem item : items ) {
                    if ( item instanceof ATDataSetAttachmentItem ) {
                        ATDataSetAttachmentItem attachmentItem = (ATDataSetAttachmentItem)item;
                        attachmentItems.add(attachmentItem);
                    }
                }
                return attachmentItems;
            }
        }
        return null;
    }

    /**
     * Sets a color value at the specified index of the attribute map
     * @param value The color to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(Color value, int attributeIndex) {
        String colorString = null;
        if ( value != null ) {
            colorString = "";
            colorString += value.getRed() + "," + value.getGreen() + "," + value.getBlue();
        }
        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(colorString));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets an int value at the specified index of the attribute map
     * @param value The int to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(int value, int attributeIndex) {
        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(value));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a list item value at the specified index of the attribute map
     * @param listItem The list item to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(ATListItem listItem, int attributeIndex) {
        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(listItem));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a location value at the specified index of the attribute map
     * @param lat The latitude as a double
     * @param lng The longitude as a double
     * @param attributeIndex The index of the attribute
     */
    public void setLocationValueForAttributeIndex(double lat, double lng, int attributeIndex) {
        String value;

        value = lat + "," + lng;
        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(value));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a string value at the specified index of the attribute map
     * @param value The string to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(String value, int attributeIndex) {

        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(value));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a boolean value at the specified index of the attribute map
     * @param value The boolean to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(boolean value, int attributeIndex) {
        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(value));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a double value at the specified index of the attribute map
     * @param value The cdouble to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(double value, int attributeIndex) {
        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(value));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a cdata set item value at the specified index of the attribute map
     * @param value The data set item to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(ATDataSetItem value, int attributeIndex) {
        mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(value));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a list of data set items at the specified index of the attribute map
     * @param value The data set items to set
     * @param attributeIndex The index of the attribute
     */
    public void setValueForAttribute(List<ATDataSetItem> value, int attributeIndex) {
        mAttributeMap.put(attributeIndex,new ATDataSetItemAttribute(value));
        updateMaxAttribute(attributeIndex);
    }

    /**
     * Sets a date value at the specified index of the attribute map
     * @param date The date to set
     * @param attributeIndex The index of the attribute
     */
    public void setDateValueForAttribute(DateTime date, int attributeIndex) {
        if ( date != null ) {
            String dateString = AppTreeDateFormat.print(date);
            mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(dateString));
        }
    }

    /**
     * Sets a date time value at the specified index of the attribute map
     * @param date The date time to set
     * @param attributeIndex The index of the attribute
     */
    public void setDateTimeValueForAttribute(DateTime date, int attributeIndex) {
        if ( date != null ) {
            String dateString = AppTreeDateTimeFormat.print(date);
            mAttributeMap.put(attributeIndex, new ATDataSetItemAttribute(dateString));
        }
    }

    /**
     * Updates the max attriubte index
     * @param attributeIndex
     */
    private void updateMaxAttribute(int attributeIndex) {
        mMaxAttributeIndex = attributeIndex > mMaxAttributeIndex ? attributeIndex : mMaxAttributeIndex;
    }

    /**
     * Gets the max attribute index
     * @return
     */
    private int maxAttributeIndex() {
        return mMaxAttributeIndex;
    }

    /**
     * Converts the data set item to a json object where the primary key does not need to be set
     * @return json object of the data set item
     */
    public JSONObject toJSON() {
        try {
            return _toJSON(false);
        } catch (ATInvalidPrimaryKeyException exception) {
            System.out.println("Caught invalid primary key exception when primaryKeyRequired was false. THIS SHOULD NOT HAPPEN");
        }
        return null;
    }

    /**
     * Converts the data set item to a json object the primary key must be set
     * @return json object of the data set item
     * @throws ATInvalidPrimaryKeyException
     */
    public JSONObject toJSONWithPrimaryKey() throws ATInvalidPrimaryKeyException {
        if ( mPrimaryKey == null ) {
            throw new ATInvalidPrimaryKeyException("Primary key can not be null for record: " + toJSON());
        }
        return _toJSON(true);
    }

    /**
     * Converts the data set item to a json object
     * @param primaryKeyRequired A boolean indicating whether or not the primary key of the data set item must be set
     * @return
     * @throws ATInvalidPrimaryKeyException
     */
    private JSONObject _toJSON(boolean primaryKeyRequired) throws ATInvalidPrimaryKeyException{
        JSONObject json;
        JSONArray attributes;
        Iterator attributeIterator;
        Map.Entry<Integer,ATDataSetItemAttribute> entry;

        json = new JSONObject();
        json.put("primaryKey",mPrimaryKey);
        json.put("CRUDStatus", mCRUDStatus);
        json.putOpt("clientKey", mClientKey);
        json.put("recordType", getItemType().stringValue);
        json.put("assessmentStatus",mAssessmentStatus.stringValue);

        attributes = new JSONArray();
        attributeIterator = mAttributeMap.entrySet().iterator();
        while ( attributeIterator.hasNext() ) {
            entry = (Map.Entry) attributeIterator.next();
            attributes.put(entry.getKey(),entry.getValue().getJSONValue(primaryKeyRequired));
        }
        json.put("attributes",attributes);
        return json;
    }

    /**
     * Updates a data set item from a json object
     * @param jsonObject Json object containing the values of the data set item
     * @param attachmentIDMap
     */
    public void updateFromJSON(JSONObject jsonObject, HashMap<String, ATAttachmentUploadItem> attachmentIDMap) {
        JSONArray attributes;
        String crudStatusString;
        String assessmentStatusString;
        Object subItem;
        JSONObject subItemJSON;
        JSONArray subAttributes;
        ATDataSetItem subDataSetItem;
        List<ATDataSetItem> subItems;
        String subClientKey;
        ATAttachmentUploadItem attachmentUploadItem;
        ATDataSetAttachmentItem attachment;
        String subRecordType;

        mPrimaryKey = JSONUtils.getString(jsonObject, "primaryKey");
        crudStatusString = JSONUtils.getString(jsonObject,"CRUDStatus");

        if ( crudStatusString != null ) {
            mCRUDStatus = CRUDStatus.fromString(crudStatusString);
        }
        assessmentStatusString = JSONUtils.getString(jsonObject,"assessmentStatus");
        if ( assessmentStatusString != null ) {
            mAssessmentStatus = AssessmentStatus.fromString(assessmentStatusString);
        }
        mClientKey = JSONUtils.getString(jsonObject,"clientKey");
        attributes = JSONUtils.getJSONArray(jsonObject, "attributes");
        if ( attributes != null ) {
            for ( int i = 0; i < attributes.length(); i++ ) {
                if ( attributes.isNull(i) ) {
                    continue;
                }
                subItem = attributes.get(i);
                if ( subItem instanceof String ) {
                    setValueForAttribute((String)subItem,i);
                } else if ( subItem instanceof JSONArray ) {
                    attachmentUploadItem = null;
                    subAttributes = (JSONArray) subItem;
                    subItems = new ArrayList<ATDataSetItem>();
                    for ( int j = 0; j < subAttributes.length(); j++ ) {
                        subItemJSON = subAttributes.getJSONObject(j);
                        subRecordType = JSONUtils.getString(subItemJSON,"recordType");
                        subClientKey = JSONUtils.getString(subItemJSON, "clientKey");
                        if ( Type.fromString(subRecordType) == Type.Attachment ) {
                            if ( subClientKey != null && attachmentIDMap != null ) {
                                attachmentUploadItem = attachmentIDMap.get(subClientKey);
                            }
                            subDataSetItem = new ATDataSetAttachmentItem();
                            subDataSetItem.updateFromJSON(subItemJSON,null);
                            if ( attachmentUploadItem != null ) {
                                ((ATDataSetAttachmentItem) subDataSetItem).setAttachmentFile(attachmentUploadItem.getFileItem());
                            }
                            subItems.add(subDataSetItem);
                        } else {
                            subDataSetItem = new ATDataSetItem();
                            subDataSetItem.updateFromJSON(subItemJSON, attachmentIDMap);
                            subItems.add(subDataSetItem);
                        }
                    }
                    setValueForAttribute(subItems,i);
                }
            }
        }
    }
}