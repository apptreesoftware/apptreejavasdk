package com.apptree.sdk.model;

import org.json.JSONObject;

/**
 * Created by Alexis Andreason on 11/6/14.
 */
public class ATServiceConfigurationAttribute {
    public enum AttributeType {
        String,
        Int,
        Double,
        Boolean,
        Date,
        DateTime,
        Image,
        Location,
        Attachments,
        Relation,
        ListItem,
        Color,
        Badge,
        None;
    }

    String name;
    boolean required, readonly;
    ATRelatedServiceConfiguration relatedService;
    ATServiceConfigurationAttribute.AttributeType attributeType;

    private int mAttributeIndex;

    /**
     * Creates a service configuration attribute
     */
    public ATServiceConfigurationAttribute() {}

    /**
     * Creates a service configuration attribute
     * @param attributeIndex Th eindex of the attribute
     */
    public ATServiceConfigurationAttribute(int attributeIndex) {
        mAttributeIndex = attributeIndex;
        this.name = "attribute" + mAttributeIndex;
    }

    /**
     * Sets the name of the attribute
     * @param name
     */
    public void setName (String name) { this.name = name; }

    /**
     * Gets the name of the attribute
     * @return
     */
    public String getName () { return name; }

    /**
     * Get the index of the attribute
     * @return
     */
    public int getAttributeIndex() {
        return mAttributeIndex;
    }

    /**
     * Sets the index of the attributes
     * @param attributeIndex
     */
    public void setAttributeIndex(int attributeIndex) {
        mAttributeIndex = attributeIndex;
    }

    /**
     * Sets the attribute type
     * @param attributeType
     */
    public void setAttributeType (AttributeType attributeType) { this.attributeType = attributeType; }

    /**
     * Sets the related service
     * @param serviceConfiguration
     */
    public void setRelatedService(ATRelatedServiceConfiguration serviceConfiguration) {
        relatedService = serviceConfiguration;
    }

    /**
     * Converts an attribute to a json object
     * @return
     */
    public JSONObject toJSON() {
        JSONObject jsonObject;

        jsonObject = new JSONObject();
        jsonObject.putOpt("name", name);
        jsonObject.putOpt("readonly", readonly);
        jsonObject.putOpt("required", required);
        jsonObject.putOpt("attributeIndex",mAttributeIndex);
        jsonObject.putOpt("attributeType", getStringForAttributeType(attributeType));
        if ( relatedService != null ) {
            jsonObject.putOpt("relatedService", relatedService.toJSON());
        }
        return jsonObject;
    }

    /**
     * Gets the attribute type from string
     * @param attributeString The type string
     * @return
     */
    public AttributeType getAttributeTypeForString(String attributeString) {

        if (attributeString.equalsIgnoreCase("Text")) {
            return AttributeType.String;
        } else if (attributeString.equalsIgnoreCase("Integer")) {
            return AttributeType.Int;
        } else if (attributeString.equalsIgnoreCase("Float")) {
            return AttributeType.Double;
        } else if (attributeString.equalsIgnoreCase("Boolean")) {
            return AttributeType.Boolean;
        } else if (attributeString.equalsIgnoreCase("Date")) {
            return AttributeType.Date;
        } else if (attributeString.equalsIgnoreCase("DateTime")) {
            return AttributeType.DateTime;
        } else if (attributeString.equalsIgnoreCase("Attachments")) {
            return AttributeType.Attachments;
        } else if (attributeString.equalsIgnoreCase("Relationship")) {
            return AttributeType.Relation;
        } else if (attributeString.equalsIgnoreCase("Location")) {
            return AttributeType.Location;
        } else if (attributeString.equalsIgnoreCase("Image")) {
            return AttributeType.Image;
        } else if (attributeString.equalsIgnoreCase("DateTime") ) {
            return AttributeType.DateTime;
        } else if (attributeString.equalsIgnoreCase("ListItem") ) {
            return AttributeType.ListItem;
        } else  if ( attributeString.equalsIgnoreCase("Badge") ) {
            return AttributeType.Badge;
        }
        else {
            return AttributeType.None;
        }
    }

    /**
     * Gets the string for the attribute type
     * @param attributeType
     * @return
     */
    public String getStringForAttributeType(AttributeType attributeType) {
        switch (attributeType) {
            case Int:
                return "Integer";
            case String:
                return "Text";
            case Double:
                return "Float";
            case Boolean:
                return "Boolean";
            case Date:
                return "Date";
            case DateTime:
                return "DateTime";
            case Attachments:
                return "Attachments";
            case Relation:
                return "Relationship";
            case Location:
                return "Location";
            case Image:
                return "Image";
            case ListItem:
                return "ListItem";
            case Color:
                return "Color";
            case Badge:
                return "Badge";
        }
        return "None";
    }

    public static class Builder {
        AttributeType mAttributeType = AttributeType.String;
        String mAttributeName;
        ATRelatedServiceConfiguration mRelatedService;
        boolean mReadOnly, mRequired;
        private int mAttributeIndex;

        /**
         * Creates a service attribute builder
         * @param attributeIndex The index of the attribute
         */
        public Builder(int attributeIndex) {
            mAttributeIndex = attributeIndex;
            mAttributeName = "attribute" + mAttributeIndex;
        }

        /**
         * Sets the name of the attribute
         * @param attributeName The attribute name
         * @return The builder with the name
         */
        public Builder name(String attributeName) {
            mAttributeName = attributeName;
            return this;
        }

        /**
         * Sets the attribute as read only
         * @return The builder with readonly parameter
         */
        public Builder readonly() {
            mReadOnly = true;
            return this;
        }

        /**
         * Sets the attribute as required
         * @return The builder with required parameter
         */
        public Builder required() {
            mRequired = true;
            return this;
        }

        /**
         * Sets the attribute type
         * @param type The type of the attribute
         * @return The builder with the attribute type
         */
        public Builder attributeType(AttributeType type) {
            mAttributeType = type;
            return this;
        }

        /**
         * Sets the attribute type as list item
         * @return The builder with list item type
         */
        public Builder asListItem() {
            mAttributeType = AttributeType.ListItem;
            return this;
        }

        /**
         * Sets the attribute type as location
         * @return the builder with location type
         */
        public Builder asLocation() {
            mAttributeType = AttributeType.Location;
            return this;
        }

        /**
         * Sets the attribute type as relationship
         * @return the builder with relationship type
         */
        public Builder asRelationship(ATRelatedServiceConfiguration relatedService) {
            mAttributeType = AttributeType.Relation;
            mRelatedService = relatedService;
            return this;
        }

        /**
         * Sets the attribute type as attachments
         * @return the builder with attachments type
         */
        public Builder asAttachments() {
            mAttributeType = AttributeType.Attachments;
            return this;
        }

        /**
         * Sets the attribute type as date time
         * @return the builder with date time type
         */
        public Builder asDateTime() {
            mAttributeType = AttributeType.DateTime;
            return this;
        }

        /**
         * Sets the attribute type as int
         * @return the builder with int type
         */
        public Builder asInt() {
            mAttributeType = AttributeType.Int;
            return this;
        }

        /**
         * Sets the attribute type as double
         * @return the builder with double type
         */
        public Builder asDouble() {
            mAttributeType = AttributeType.Double;
            return this;
        }

        /**
         * Sets the attribute type as date
         * @return the builder with date type
         */
        public Builder asDate() {
            mAttributeType = AttributeType.Date;
            return this;
        }

        /**
         * Sets the attribute type as bool
         * @return the builder with bool type
         */
        public Builder asBool() {
            mAttributeType = AttributeType.Boolean;
            return this;
        }

        /**
         * Sets the attribute type as color
         * @return the builder with color type
         */
        public Builder asColor() {
            mAttributeType = AttributeType.Color;
            return this;
        }

        /**
         * Sets the attribute type as badge
         * @return the builder with badge type
         */
        public Builder asBadge() {
            mAttributeType = AttributeType.Badge;
            return this;
        }

        /**
         * Builds a service configuration attribute from the builder parameters
         * @return service configuration attribute
         */
        public ATServiceConfigurationAttribute build() {
            ATServiceConfigurationAttribute attribute = new ATServiceConfigurationAttribute(mAttributeIndex);
            attribute.setName(mAttributeName);
            attribute.setAttributeType(mAttributeType);
            attribute.readonly = mReadOnly;
            attribute.required = mRequired;
            attribute.setRelatedService(mRelatedService);
            return attribute;
        }
    }
}
