package com.apptree.sdk.model;

import org.json.JSONObject;

import java.util.HashSet;
import java.util.List;

/**
 * Created by Matthew Smith, AppTree Software LLC on 4/18/15.
 */
public class ATAssessmentConfiguration extends ATServiceConfiguration {

    boolean mSupportsMove;

    /**
     * Create an assessment configuration
     * @param name The name of the assessment
     * @param supportsMove A boolean indicating whether it supports moving items
     * @param attributes A List of service configuration attributes for the assessment
     */
    public ATAssessmentConfiguration(String name, boolean supportsMove, List<ATServiceConfigurationAttribute> attributes) {
        super(name, attributes, null, null, null);
        mSupportsMove = supportsMove;
    }

    /**
     * Converts the assessment configuration to a json object
     * @return JSONObject for the assessment configuration
     */
    public JSONObject toJSON() {
        JSONObject json = super.toJSON();
        json.put("supportsMove", mSupportsMove);
        json.put("isAuditService", true);
        return json;
    }

    public static class Builder {
        String mName;
        boolean mSupportsMove;
        private List<ATServiceConfigurationAttribute> mUpdateAttributes;

        /**
         * Creates an assessment builder
         * @param serviceName The name of the assessment being built
         */
        public Builder(String serviceName) {
            mName = serviceName;
        }

        /**
         * Updates the service configuration attributes
         * @param attributes The service configuration attributes
         * @return The builder with the specified updates
         */
        public Builder withUpdateAttributes(List<ATServiceConfigurationAttribute> attributes) {
            mUpdateAttributes = attributes;
            return this;
        }

        /**
         * Updates a boolean indicating if moving is supported
         * @return the builder with the move indicator
         */
        public Builder supportsMove() {
            mSupportsMove = true;
            return this;
        }

        /**
         * Builds an assessment configuration with the builder specified attributes, move indicator, and name
         * @return
         * @throws InvalidServiceAttributeException
         */
        public ATAssessmentConfiguration build() throws InvalidServiceAttributeException {
            int index;
            if ( (index = checkIndexUniqueness(mUpdateAttributes)) != -1 ) {
                throw new InvalidServiceAttributeException("Your update attributes contain an index that is not unique: Index " + index);
            }
            return new ATAssessmentConfiguration(mName, mSupportsMove, mUpdateAttributes);
        }

        /**
         * Check the service configuration attribute indexes for uniqueness
         * @param attributes
         * @return
         */
        private int checkIndexUniqueness(List<ATServiceConfigurationAttribute> attributes) {
            if ( attributes == null ) {
                return -1;
            }
            HashSet<Integer> indexes = new HashSet<Integer>();
            for ( ATServiceConfigurationAttribute attribute : attributes ) {
                if ( indexes.contains(attribute.getAttributeIndex()) ) {
                    return attribute.getAttributeIndex();
                }
                indexes.add(attribute.getAttributeIndex());
            }
            return -1;
        }
    }

    public static class InvalidServiceAttributeException extends Exception {

        /**
         * An invalid service attribute excpetion extends exception
         * @param message
         */
        public InvalidServiceAttributeException(String message) {
            super(message);
        }
    }
}
