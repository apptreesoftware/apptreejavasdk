package com.apptree.sdk.model;

import org.json.JSONObject;

/**
 * Created by matthew on 1/5/15.
 */

public abstract class ATListServiceConfiguration {
    enum ListType {
        Search,
        Cache
    }

    String mListName;
    boolean mIncludesLocation;
    boolean mAuthenticationRequired;
    ListType mListType;
    String mAttribute1Description;
    String mAttribute2Description;
    String mAttribute3Description;
    String mAttribute4Description;
    String mAttribute5Description;

    /**
     * Returns the list name
     * @return
     */
    public String getListName() {
        return mListName;
    }

    /**
     * Sets the list name
     * @param listName
     */
    public void setListName(String listName) {
        mListName = listName;
    }

    /**
     * Returns a boolean indicating if the list includes location
     * @return
     */
    public boolean isIncludesLocation() {
        return mIncludesLocation;
    }

    /**
     * Sets a boolean indicating if the list includes location
     * @param includesLocation
     */
    public void setIncludesLocation(boolean includesLocation) {
        mIncludesLocation = includesLocation;
    }

    /**
     * Returns attribute 1 description
     * @return
     */
    public String getAttribute1Description() {
        return mAttribute1Description;
    }

    /**
     * Sets attribute 1 description
     * @param attribute1Description
     */
    public void setAttribute1Description(String attribute1Description) {
        mAttribute1Description = attribute1Description;
    }

    /**
     * Gets the attribute 2 description
     * @return
     */
    public String getAttribute2Description() {
        return mAttribute2Description;
    }

    /**
     * Sets attribute 2 description
     * @param attribute2Description
     */
    public void setAttribute2Description(String attribute2Description) {
        mAttribute2Description = attribute2Description;
    }

    /**
     * Gets the attribute 3 description
     * @return
     */
    public String getAttribute3Description() {
        return mAttribute3Description;
    }

    /**
     * Sets attribute 3 description
     * @param attribute3Description
     */
    public void setAttribute3Description(String attribute3Description) {
        mAttribute3Description = attribute3Description;
    }

    /**
     * Gets the attribute 4 description
     * @return
     */
    public String getAttribute4Description() {
        return mAttribute4Description;
    }

    /**
     * Sets attribute 4 description
     * @param attribute4Description
     */
    public void setAttribute4Description(String attribute4Description) {
        mAttribute4Description = attribute4Description;
    }

    /**
     * Gets the attribute 5 description
     * @return
     */
    public String getAttribute5Description() {
        return mAttribute5Description;
    }

    /**
     * Sets attribute 5 description
     * @param attribute5Description
     */
    public void setAttribute5Description(String attribute5Description) {
        mAttribute5Description = attribute5Description;
    }

    /**
     * Gets a boolean indicating whether authentication is required for this list
     * @return
     */
    public boolean isAuthenticationRequired() {
        return mAuthenticationRequired;
    }

    /**
     * Sets the boolean indicating whether authentication is required for this list
     * @param authenticationRequired
     */
    public void setAuthenticationRequired(boolean authenticationRequired) {
        mAuthenticationRequired = authenticationRequired;
    }

    /**
     * Converts the list to a json object
     * @return
     */
    public JSONObject toJSON() {
        JSONObject jsonObject;

        jsonObject = new JSONObject();
        jsonObject.putOpt("listName",mListName);
        jsonObject.putOpt("includesLocation",mIncludesLocation);
        jsonObject.putOpt("attribute1Description", mAttribute1Description);
        jsonObject.putOpt("attribute2Description", mAttribute2Description);
        jsonObject.putOpt("attribute3Description", mAttribute3Description);
        jsonObject.putOpt("attribute4Description", mAttribute4Description);
        jsonObject.putOpt("attribute5Description", mAttribute5Description);
        if ( mListType == ListType.Search ) {
            jsonObject.put("listType", "search");
        } else if ( mListType == ListType.Cache ) {
            jsonObject.put("listType", "cache");
        }
        jsonObject.put("authenticationRequired",mAuthenticationRequired);
        return jsonObject;
    }
}
