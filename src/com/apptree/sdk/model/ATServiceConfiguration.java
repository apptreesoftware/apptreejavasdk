package com.apptree.sdk.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Alexis Andreason on 11/7/14.
 */
public class ATServiceConfiguration {
    private String configurationName;
    private List<ATServiceConfigurationAttribute> mUpdateAttributes;
    private List<ATServiceConfigurationAttribute> mCreateAttributes;
    private List<ATServiceConfigurationAttribute> mSearchAttributes;
    private List<ATServiceParameter> mUpdateParameters;

    /**
     * Creates a service configuration
     */
    public ATServiceConfiguration() {
    }

    /**
     * Creates a service configuration
     * @param configurationName The configuration name
     * @param updateAttributes The attributes used to update/view
     * @param createAttributes The attributes used to create
     * @param searchAttributes The attributes used to search
     */
    public ATServiceConfiguration(String configurationName, List<ATServiceConfigurationAttribute> updateAttributes, List<ATServiceConfigurationAttribute> createAttributes, List<ATServiceConfigurationAttribute> searchAttributes, List<ATServiceParameter> updateParameters) {
        this.configurationName = configurationName;
        this.mUpdateAttributes = updateAttributes;
        mCreateAttributes = createAttributes;
        mSearchAttributes = searchAttributes;
        mUpdateParameters = updateParameters;
    }

    /**
     * Converts the service configuration to a json object
     * @return
     */
    public JSONObject toJSON() {
        JSONObject jsonObject;
        JSONArray attributeArray;

        jsonObject = new JSONObject();
        jsonObject.putOpt("name", configurationName);
        if (mCreateAttributes != null) {
            attributeArray = new JSONArray();
            mCreateAttributes.sort(new ATServiceAttributeComparator());
            for (ATServiceConfigurationAttribute atConfigurationAttribute : mCreateAttributes) {
                attributeArray.put(atConfigurationAttribute.toJSON());
            }
            jsonObject.put("createAttributes",attributeArray);

        }
        if (mUpdateAttributes != null) {
            attributeArray = new JSONArray();
            mUpdateAttributes.sort(new ATServiceAttributeComparator());
            for (ATServiceConfigurationAttribute atConfigurationAttribute : mUpdateAttributes) {
                attributeArray.put(atConfigurationAttribute.toJSON());
            }
            jsonObject.put("updateAttributes",attributeArray);
        }
        if (mSearchAttributes != null) {
            attributeArray = new JSONArray();
            mSearchAttributes.sort(new ATServiceAttributeComparator());
            for (ATServiceConfigurationAttribute atConfigurationAttribute : mSearchAttributes) {
                attributeArray.put(atConfigurationAttribute.toJSON());
            }
            jsonObject.put("searchAttributes",attributeArray);
        }
        if ( mUpdateParameters != null ) {
            attributeArray = new JSONArray();
            for ( ATServiceParameter parameter : mUpdateParameters ) {
                attributeArray.put(parameter.toJSON());
            }
            jsonObject.put("updateParameters", attributeArray);
        }


        return jsonObject;
    }

    private static class ATServiceAttributeComparator implements Comparator<ATServiceConfigurationAttribute> {

        @Override
        public int compare(ATServiceConfigurationAttribute o1, ATServiceConfigurationAttribute o2) {
            return o1.getAttributeIndex() - o2.getAttributeIndex();
        }
    }

    public static class Builder {
        String mName;
        private List<ATServiceConfigurationAttribute> mUpdateAttributes;
        private List<ATServiceConfigurationAttribute> mCreateAttributes;
        private List<ATServiceConfigurationAttribute> mSearchAttributes;
        private List<ATServiceParameter> mUpdateParameters;

        /**
         * Creates a service configuration builder
         * @param serviceName The configuration name
         */
        public Builder(String serviceName) {
            mName = serviceName;
        }

        /**
         * Adds the update attributes to the builder
         * @param attributes The attributes to update
         * @return The builder with update attributes
         */
        public Builder withUpdateAttributes(List<ATServiceConfigurationAttribute> attributes) {
            mUpdateAttributes = attributes;
            return this;
        }

        /**
         * Adds the create attributes to the builder
         * @param attributes The attributes to create
         * @return The builder with create attributes
         */
        public Builder withCreateAttributes(List<ATServiceConfigurationAttribute> attributes) {
            mCreateAttributes = attributes;
            return this;
        }

        /**
         * Adds the search attributes to the builder
         * @param attributes The attributes to search
         * @return The builder with search attributes
         */
        public Builder withSearchAttributes(List<ATServiceConfigurationAttribute> attributes) {
            mSearchAttributes = attributes;
            return this;
        }

        public Builder withUpdateParameters(List<ATServiceParameter> parameters) {
            mUpdateParameters = parameters;
            return this;
        }

        /**
         * Creates a service configuration with the specified builder parameters
         * @return
         * @throws InvalidServiceAttributeException
         */
        public ATServiceConfiguration build() throws InvalidServiceAttributeException {
            int index;
            if ( (index = checkIndexUniqueness(mUpdateAttributes)) != -1 ) {
                throw new InvalidServiceAttributeException("Your update attributes contain an index that is not unique: Index " + index);
            }
            if ( (index = checkIndexUniqueness(mCreateAttributes)) != -1 ) {
                throw new InvalidServiceAttributeException("Your create attributes contain an index that is not unique: Index " + index);
            }
            if ( (index = checkIndexUniqueness(mSearchAttributes)) != -1 ) {
                throw new InvalidServiceAttributeException("Your search attributes contain an index that is not unique: Index " + index);
            }
            return new ATServiceConfiguration(mName,mUpdateAttributes,mCreateAttributes,mSearchAttributes,mUpdateParameters);
        }

        private int checkIndexUniqueness(List<ATServiceConfigurationAttribute> attributes) {
            if ( attributes == null ) {
                return -1;
            }
            HashSet<Integer> indexes = new HashSet<Integer>();
            for ( ATServiceConfigurationAttribute attribute : attributes ) {
                if ( indexes.contains(attribute.getAttributeIndex()) ) {
                    return attribute.getAttributeIndex();
                }
                indexes.add(attribute.getAttributeIndex());
            }
            return -1;
        }
    }

    public static class InvalidServiceAttributeException extends Exception {

        public InvalidServiceAttributeException(String message) {
            super(message);
        }
    }
}
