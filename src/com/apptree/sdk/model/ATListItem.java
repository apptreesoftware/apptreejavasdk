package com.apptree.sdk.model;

import com.apptree.sdk.util.JSONUtils;
import org.json.JSONObject;

/**
 * Created by matthew on 11/5/14.
 */
public class ATListItem {

    private String id;
    private String parentID;
    public int orderBy = -1;
    private String value;
    private String attribute01;
    private String attribute02;
    private String attribute03;
    private String attribute04;
    private String attribute05;
    private String latitude;
    private String longitude;

    public ATListItem(String value) {
        this.value = value;
        this.id = value;
    }


    //  Create By Eddie
    public ATListItem(){

    }

    public ATListItem(String value, String id) {
        this.value = value;
        this.id = id;
    }

    public ATListItem(String value, String id, String parentID) {
        this.value = value;
        this.id = id;
        this.parentID=parentID;
    }



    public static class Attribute{
        public static int
                ATTRIBUTE_1 = 0,
                ATTRIBUTE_2 = 1,
                ATTRIBUTE_3=2,
                ATTRIBUTE_4=3,
                ATTRIBUTE_5=4;

    }

    //End of Create by Eddie

    public ATListItem(JSONObject json) {
        updateFromJSON(json);
    }

    /**
     * Converts the list item to a string
     * @return
     */
    public String toString() {
        return "#" + value + "#" + attribute01 + "#" + attribute02 + "#" + attribute03 + "#" + attribute04 + "#" + attribute05 + "$" + orderBy;
    }

    /**
     * Sets the list item attribute
     * @param value The string list item attribute value
     * @param index The attribute to be set 1-5
     */
    public void setAttributeForIndex(String value, int index) {
        switch (index) {
            case 0:
                attribute01 = value;
                break;
            case 1:
                attribute02 = value;
                break;
            case 2:
                attribute03 = value;
                break;
            case 3:
                attribute04 = value;
                break;
            case 4:
                attribute05 = value;
                break;
            default:
                System.out.println("The index you specified (" + index + ") is beyond the allowed number of attributes ( 0 - 4 )");
        }
    }

    /**
     * Sets the list item attribute
     * @param intValue The int value
     * @param index The attribute to be set 1-5
     */
    public void setAttributeForIndex(int intValue, int index) {
        String value = intValue + "";
        switch (index) {
            case 0:
                attribute01 = value;
                break;
            case 1:
                attribute02 = value;
                break;
            case 2:
                attribute03 = value;
                break;
            case 3:
                attribute04 = value;
                break;
            case 4:
                attribute05 = value;
                break;
            default:
                System.out.println("The index you specified (" + index + ") is beyond the allowed number of attributes ( 0 - 4 )");
        }
    }

    /**
     * Sets the list item attribute
     * @param dataSetItem The data set item value
     * @param index The attribute to be set 1-5
     */
    public void setAttributeForIndex(ATDataSetItem dataSetItem, int index) {
        String stringValue;
        stringValue = dataSetItem.toJSON().toString();
        setAttributeForIndex(stringValue,index);
    }

    /**
     * Sets the lit item attribute
     * @param listItem The list item value
     * @param index The attribute to be set 1-5
     */
    public void setAttributeForIndex(ATListItem listItem, int index) {
        String stringValue;
        stringValue = listItem.toJSON().toString();
        setAttributeForIndex(stringValue,index);
    }

    /**
     * REturns the attribute
     * @param index The attribute to get 1-5
     * @return
     */
    public String getAttributeForIndex(int index) {
        switch(index) {
            case 0:
                return attribute01;
            case 1:
                return attribute02;
            case 2:
                return attribute03;
            case 3:
                return attribute04;
            case 4:
                return attribute05;
            default:
                System.out.println("Requesting an attribute index ( " + index + ") that is beyond the allowed attribute indexes ( 0 - 4 )");
        }
        return null;
    }

    /**
     * Converts the list item to a json object
     * @return A json object
     */
    public JSONObject toJSON() {
        JSONObject json;
        json = new JSONObject();
        json.put("value",value);
        json.put("id", id);
        json.putOpt("attribute01", attribute01);
        json.putOpt("attribute02", attribute02);
        json.putOpt("attribute03", attribute03);
        json.putOpt("attribute04", attribute04);
        json.putOpt("attribute05", attribute05);
        json.putOpt("latitude",latitude);
        json.putOpt("longitude",longitude);
        json.putOpt("parentID",this.parentID);
        if ( orderBy != -1 ) {
            json.put("orderBy",orderBy);
        }
        return json;
    }

    /**
     * Updates a list item from a json object
     * @param json The json object containing the list item attributes
     */
    public void updateFromJSON(JSONObject json) {
        value = JSONUtils.getString(json,"value");
        id = json.optString("id", value);
        attribute01 = JSONUtils.getString(json,"attribute01");
        attribute02 = JSONUtils.getString(json,"attribute02");
        attribute03 = JSONUtils.getString(json,"attribute03");
        attribute04 = JSONUtils.getString(json,"attribute04");
        attribute05 = JSONUtils.getString(json,"attribute05");
        latitude = JSONUtils.getString(json,"latitude");
        longitude = JSONUtils.getString(json,"longitude");
        parentID = JSONUtils.getString(json,"parentValue");
        orderBy = JSONUtils.getInt(json,"orderBy");
    }
}
