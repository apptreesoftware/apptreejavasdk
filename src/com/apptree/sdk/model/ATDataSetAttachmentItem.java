package com.apptree.sdk.model;

import org.apache.commons.fileupload.FileItem;

/**
 * Created by matthew on 1/20/15.
 */


public class ATDataSetAttachmentItem extends ATDataSetItem {

    private static int AttachmentAttributeTitle = 1;
    private static int AttachmentAttributeLink = 2;
    private static int AttachmentAttributeImage = 3;
    private static int AttachmentAttributeText = 4;
    private static int AttachmentAttributeTextEntity = 5;
    private static int AttachmentAttributeVideo = 6;
    private static int AttachmentAttributeFile = 7;
    private static int AttachmentAttributeEditable = 8;
    private static int AttachmentAttributeDeletable = 9;

    FileItem attachmentFileItem;

    /**
     * Sets the file item
     * @param fileItem
     */
    public void setAttachmentFile(FileItem fileItem) {
        attachmentFileItem = fileItem;
    }

    /**
     * Gets the file item
     * @return attachment file item
     */
    public FileItem getAttachmentFile() {
        return attachmentFileItem;
    }

    /**
     * Sets the attachment URL used when the attachment is an image
     * @param imageAttachmentURL The url for the attachment stream
     */
    public void setImageAttachmentURL(String imageAttachmentURL) {
        setValueForAttribute(imageAttachmentURL,AttachmentAttributeImage);
    }

    /**
     * Gets the image attachment URL
     * @return
     */
    public String getImageAttachmentURL() {
        return getStringAttributeAtIndex(AttachmentAttributeImage);
    }

    /**
     * Sets the text when the attachment is a note
     * @param note The url for the attachment stream
     */
    public void setNoteAttachment(String note) {
        setValueForAttribute(note,AttachmentAttributeText);
    }

    /**
     * Gets the note
     * @return
     */
    public String getNoteAttachment() {
        return getStringAttributeAtIndex(AttachmentAttributeText);
    }

    /**
     * Sets the link string when the attachment is a link
     * @param link
     */
    public void setLinkAttachment(String link) {
        setValueForAttribute(link,AttachmentAttributeLink);
    }

    /**
     * gets the link
     * @return
     */
    public String getLinkAttachment() {
        return getStringAttributeAtIndex(AttachmentAttributeLink);
    }

    /**
     * Sets the attachment URL used when the attachment is an video
     * @param videoURL The url for the attachment stream
     */
    public void setVideoAttachmentURL(String videoURL) {
        setValueForAttribute(videoURL,AttachmentAttributeVideo);
    }

    /**
     * Gets the video attachment url
     * @return
     */
    public String getVideoAttachmentURL() {
        return getStringAttributeAtIndex(AttachmentAttributeVideo);
    }

    /**
     * Sets the attachment URL used when the attachment is a file
     * @param fileAttachmentURL The url for the attachment stream
     */
    public void setFileAttachmentURL(String fileAttachmentURL) {
        setValueForAttribute(fileAttachmentURL,AttachmentAttributeFile);
    }

    /**
     * Gets the attachment URL used when the attachment is a file
     */
    public String getFileAttachmentURL() {
        return getStringAttributeAtIndex(AttachmentAttributeFile);
    }

    /**
     * Sets the title of the attachment
     * @param title
     */
    public void setTitle(String title) {
        setValueForAttribute(title,AttachmentAttributeTitle);
    }

    /**
     * Gets the title
     * @return
     */
    public String getTitle() {
        return getStringAttributeAtIndex(AttachmentAttributeTitle);
    }

    /**
     * Returns the attachment type
     * @return
     */
    public Type getItemType() {
        return Type.Attachment;
    }

}
