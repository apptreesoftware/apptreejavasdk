package com.apptree.sdk.model;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by alexis on 9/1/15.
 */
public class ATServiceParameter {
    public enum ParameterType {
        String,
        Boolean,
        SingleList,
        MultiList;
    }

    String key;
    ParameterType parameterType;
    List<String> possibleValues;

    public ATServiceParameter() {}

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setParameterType(ParameterType parameterType) {
        this.parameterType = parameterType;
    }

    public ParameterType getParameterType() {
        return parameterType;
    }

    public void addPossibleValue(String possibleValue) {
        if ( possibleValues == null ) {
            possibleValues = new ArrayList<String>();
        }
        possibleValues.add(possibleValue);
    }

    public void addPossibleValues(Collection<String> possibleValues) {
        if ( possibleValues != null ) {
            if ( this.possibleValues == null ) {
                this.possibleValues = new ArrayList<String>();
            }
            this.possibleValues.addAll(possibleValues);
        }
    }

    public List<String> getPossibleValues() {
        return possibleValues;
    }

    public ParameterType getParameterTypeFromString(String parameterString) {
        if (parameterString.equals("Boolean")) {
            return ParameterType.Boolean;
        } else if (parameterString.equals("SingleList")) {
            return ParameterType.SingleList;
        } else if (parameterString.equals("MultiList")) {
            return ParameterType.MultiList;
        } else {
            return ParameterType.String;
        }
    }

    public String getStringFromParameterType(ParameterType type) {
        switch(type) {
            case Boolean:
                return "Boolean";
            case SingleList:
                return "SingleList";
            case MultiList:
                return "MultiList";
            default:
                return "String";
        }
    }

    public JSONObject toJSON() {
        JSONObject jsonObject;

        jsonObject = new JSONObject();
        jsonObject.putOpt("key", key);
        jsonObject.putOpt("type", getStringFromParameterType(parameterType));
        jsonObject.putOpt("possibleValues", possibleValues);

        return jsonObject;
    }

    public static class Builder {
        ParameterType mParameterType;
        String mKey;
        List<String> mPossibleValues;

        public Builder(String key) {
            mKey = key;
        }

        public Builder asTextParameter() {
            mParameterType = ParameterType.String;
            mPossibleValues = null;
            return this;
        }

        public Builder asBooleanParameter() {
            mParameterType = ParameterType.Boolean;
            mPossibleValues = null;
            return this;
        }

        public Builder asSingleListParameter(String possibleValue) {
            mParameterType = ParameterType.SingleList;
            mPossibleValues = new ArrayList<String>();
            mPossibleValues.add(possibleValue);
            return this;
        }

        public Builder asMultiListParameter(Collection<String> possibleValues) {
            mParameterType = ParameterType.MultiList;
            mPossibleValues = new ArrayList<String>();
            mPossibleValues.addAll(possibleValues);
            return this;
        }

        public ATServiceParameter build() {
            ATServiceParameter parameter = new ATServiceParameter();
            parameter.setKey(mKey);
            parameter.setParameterType(mParameterType);
            parameter.addPossibleValues(mPossibleValues);
            return parameter;
        }
    }
}
